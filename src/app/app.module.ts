import { AngularEditorModule } from '@kolkov/angular-editor';
import { NgxPrintModule } from 'ngx-print';
import { ReportsComponent } from './admin/pages/reports/reports.component';
import { InventoryDashboardComponent } from './admin/pages/inventory-dashboard/inventory-dashboard.component';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/material/material.module';
import { HeaderNavigationComponent } from './navigation/header-navigation/header-navigation.component';
import { BodyComponent } from './navigation/body/body.component';
import { DashboardComponent } from './admin/pages/dashboard/dashboard.component';
import { UserInfoComponent } from './admin/pages/user-info/user-info.component';
import { AddUserDialogComponent } from './admin/pages/user-info/add-user-dialog/add-user-dialog.component';
import { DeleteUserDialogComponent } from './admin/pages/user-info/delete-user-dialog/delete-user-dialog.component';
import { EditUserDialogComponent } from './admin/pages/user-info/edit-user-dialog/edit-user-dialog.component';
import { LoginComponent } from './static/login/login.component';
import { RegisterComponent } from './static/register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MedicineInfoComponent } from './admin/pages/medicine-info/medicine-info.component';
import { PovertyInfoComponent } from './admin/pages/poverty-info/poverty-info.component';
import { AddPovertyDialogComponent } from './admin/pages/poverty-info/add-poverty-dialog/add-poverty-dialog.component';
import { UserAccountComponent } from './admin/pages/user-account/user-account.component';
import { LogoutComponent } from './static/logout/logout.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';
import { authInterceptorProviders } from './static/static-interceptor/static-interceptor.interceptor';
import { SeniorLandingInfoComponent } from './static/senior-landing-info/senior-landing-info.component';
import { PregnantLandingInfoComponent } from './static/pregnant-landing-info/pregnant-landing-info.component';
import { ChildLandingInfoComponent } from './static/child-landing-info/child-landing-info.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderNavigationComponent,
    BodyComponent,
    DashboardComponent,
    UserInfoComponent,
    AddUserDialogComponent,
    DeleteUserDialogComponent,
    EditUserDialogComponent,
    LoginComponent,
    InventoryDashboardComponent,
    RegisterComponent,
    MedicineInfoComponent,
    PovertyInfoComponent,
    AddPovertyDialogComponent,
    UserAccountComponent,
    LogoutComponent,
    ReportsComponent,
    SeniorLandingInfoComponent,
    PregnantLandingInfoComponent,
    ChildLandingInfoComponent,


  ],
  imports: [
    AngularEditorModule,
    NgxPrintModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    // NgxPrintModule.forRoot({printOpenWindow: true}),
  ],
  exports:[
    NgxSpinnerModule,
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AppModule { }
