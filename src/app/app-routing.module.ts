import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderNavigationComponent } from './navigation/header-navigation/header-navigation.component';
import { ChildLandingInfoComponent } from './static/child-landing-info/child-landing-info.component';
import { LoginComponent } from './static/login/login.component'; //login
import { PregnantLandingInfoComponent } from './static/pregnant-landing-info/pregnant-landing-info.component';
import { SeniorLandingInfoComponent } from './static/senior-landing-info/senior-landing-info.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },

  {
    path: 'pages',
    loadChildren: () =>
      import('./admin/admin-module.module').then((m) => m.AdminModuleModule),
  },
  // { path: '', loadChildren: () => import('./admin-routes/admin.module').then(m => m.AdminModule) },
  { path: 'login', component: LoginComponent },
  { path: 'senior-landing-page', component: SeniorLandingInfoComponent },
  { path: 'pregnant-landing-page', component: PregnantLandingInfoComponent },
  { path: 'child-landing-page', component: ChildLandingInfoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
