import { AnnouncementListComponent } from './pages/announcement-list/announcement-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderNavigationComponent } from '../navigation/header-navigation/header-navigation.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { InventoryDashboardComponent } from './pages/inventory-dashboard/inventory-dashboard.component';
import { MedicineInfoComponent } from './pages/medicine-info/medicine-info.component';
import { PatientListComponent } from './pages/patient/patient-list/patient-list.component';
import { PovertyInfoComponent } from './pages/poverty-info/poverty-info.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { UserAccountComponent } from './pages/user-account/user-account.component';
import { UserInfoComponent } from './pages/user-info/user-info.component';
import { PovertyReportPageComponent } from './pages/reports/poverty-report-page/poverty-report-page.component';

const routes: Routes = [


  // {path: '/admin', redirectTo:'dashboard', pathMatch:'full'},

  { path: 'admin', component: HeaderNavigationComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'patient-list', component: PatientListComponent},
      {path: 'user-info', component: UserInfoComponent},
      {path: 'announcement', component: AnnouncementListComponent},
      {path: 'medicine-info', component: MedicineInfoComponent},
      {path: 'poverty-info', component: PovertyInfoComponent},
      {path: 'user-account', component: UserAccountComponent},
      {path: 'inventory-dashboard', component: InventoryDashboardComponent},
      {path: 'reports', component: ReportsComponent},
      {path: 'poverty-reports', component: PovertyReportPageComponent},


    ]
  },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminModuleRoutingModule { }
