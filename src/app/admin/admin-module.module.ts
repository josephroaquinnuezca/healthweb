import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminModuleRoutingModule } from './admin-module-routing.module';
import { AddMedicineDialogComponent } from './pages/medicine-info/add-medicine-dialog/add-medicine-dialog.component';
import { UpdateMedicineDialogComponent } from './pages/medicine-info/update-medicine-dialog/update-medicine-dialog.component';
import { DeleteMedicineDialogComponent } from './pages/medicine-info/delete-medicine-dialog/delete-medicine-dialog.component';
import { MaterialModule } from '../shared/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';
import { ViewUserDialogComponent } from './pages/user-info/view-user-dialog/view-user-dialog.component';
import { ViewPovertyDialogComponent } from './pages/poverty-info/view-poverty-dialog/view-poverty-dialog.component';
import { ViewMedicineDialogComponent } from './pages/medicine-info/view-medicine-dialog/view-medicine-dialog.component';
import { RegisterAccountComponent } from './pages/user-account/register-account/register-account.component';
import { NgxPrintModule } from 'ngx-print';
import { EditPovertyInfoComponent } from './pages/poverty-info/edit-poverty-info/edit-poverty-info.component';
import { UpdateChildDialogComponent } from './pages/user-info/view-user-dialog/update-child-dialog/update-child-dialog.component';
import { ViewAdminUsersComponent } from './pages/user-account/view-admin-users/view-admin-users.component';
import { DeleteAdminUsersComponent } from './pages/user-account/delete-admin-users/delete-admin-users.component';
import { EditAdminUsersComponent } from './pages/user-account/edit-admin-users/edit-admin-users.component';
import { UpdateFemaleMensComponent } from './pages/user-info/view-user-dialog/update-female-mens/update-female-mens.component';
import { AddPregMaintenanceComponent } from './pages/user-info/view-user-dialog/update-female-mens/add-preg-maintenance/add-preg-maintenance.component';
import { AddMaintenanceDialogComponent } from './pages/user-info/view-user-dialog/add-maintenance-dialog/add-maintenance-dialog.component';
import { DeletePovertyDialogComponent } from './pages/poverty-info/delete-poverty-dialog/delete-poverty-dialog.component';
import { PatientListComponent } from './pages/patient/patient-list/patient-list.component';
import { PatientAddDialogComponent } from './pages/patient/patient-add-dialog/patient-add-dialog.component';
import { AnnouncementListComponent } from './pages/announcement-list/announcement-list.component';
import { SendAnnouncementDialogComponent } from './pages/announcement-list/dialog/send-announcement-dialog/send-announcement-dialog.component';
import { SendAnnouncementSeniorComponent } from './pages/announcement-list/dialog/send-announcement-senior/send-announcement-senior.component';
import { SendAnnouncementPregnantComponent } from './pages/announcement-list/dialog/send-announcement-pregnant/send-announcement-pregnant.component';
import { UpdatePregnantDialogComponent } from './pages/user-info/update-pregnant-dialog/update-pregnant-dialog.component';
import { PovertyReportPageComponent } from './pages/reports/poverty-report-page/poverty-report-page.component';
import { ConfirmAppointmentDialogComponent } from './pages/user-info/view-user-dialog/confirm-appointment-dialog/confirm-appointment-dialog.component';
import { DeleteAppointmentDialogComponent } from './pages/user-info/view-user-dialog/delete-appointment-dialog/delete-appointment-dialog.component';
import { SendAnnouncementPwdComponent } from './pages/announcement-list/dialog/send-announcement-pwd/send-announcement-pwd.component';
import { SendAnnouncementFourpsComponent } from './pages/announcement-list/dialog/send-announcement-fourps/send-announcement-fourps.component';
import { PatientViewComponent } from './pages/patient/patient-view/patient-view.component';
import { DeceasedDialogComponent } from './pages/user-info/deceased-dialog/deceased-dialog.component';

// import { AngularEditorModule } from '@kolkov/angular-editor/lib/angular-editor.module';



@NgModule({
  declarations: [
    AddMedicineDialogComponent,
    UpdateMedicineDialogComponent,
    DeleteMedicineDialogComponent,
    ViewUserDialogComponent,
    ViewPovertyDialogComponent,
    ViewMedicineDialogComponent,
    RegisterAccountComponent,
    EditPovertyInfoComponent,
    UpdateChildDialogComponent,
    ViewAdminUsersComponent,
    DeleteAdminUsersComponent,
    EditAdminUsersComponent,
    UpdateFemaleMensComponent,
    AddPregMaintenanceComponent,
    AddMaintenanceDialogComponent,
    DeletePovertyDialogComponent,
    PatientListComponent,
    PatientAddDialogComponent,
    AnnouncementListComponent,
    SendAnnouncementDialogComponent,
    SendAnnouncementSeniorComponent,
    SendAnnouncementPregnantComponent,
    UpdatePregnantDialogComponent,
    PovertyReportPageComponent,
    ConfirmAppointmentDialogComponent,
    DeleteAppointmentDialogComponent,
    SendAnnouncementPwdComponent,
    SendAnnouncementFourpsComponent,
    PatientViewComponent,
    DeceasedDialogComponent,
  ],
  imports: [
    // AngularEditorModule,
    // NgxPrintModule.forRoot({printOpenWindow: true}),
    NgxPrintModule,
    HttpClientModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    MaterialModule,
    CommonModule,
    AdminModuleRoutingModule,
  ],
  // providers: [authInterceptorProviders],
  exports: [NgxSpinnerModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AdminModuleModule {}
