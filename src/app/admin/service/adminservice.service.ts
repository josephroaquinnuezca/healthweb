import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';

import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {


  // HTTP_API_URL_PRIVATE = "http://localhost:4100/v1/private/users/";
  // HTTP_API_URL_PUBLIC = "http://localhost:4100/v1/public/users/";


  HTTP_API_URL_PRIVATE = "https://health-care-api.onrender.com/v1/private/users/";
  HTTP_API_URL_PUBLIC = "https://health-care-api.onrender.com/v1/public/users/";

  constructor(private http: HttpClient) { }

  //remaining stocks
  getRemainingStocks(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/medication-remaining-stocks`);
  }

  getAllPwd(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/get-all-pwd`);
  }

  getallFourps(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/get-all-fourps`);
  }

  //pwd

  createAnnouncementpregnant(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}/send-bulk-pregnant`;
    return this.http.post(API_URL, data)
  }

  createAnnouncementsenior(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}/send-bulk-senior`;
    return this.http.post(API_URL, data)
  }

  createAnnouncement(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}/send-bulk-malnourished`;
    return this.http.post(API_URL, data)
  }


  createAnnouncementpwd(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}/send-bulk-pwd`;
    return this.http.post(API_URL, data)
  }

  createAnnouncementfourps(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}/send-bulk-fourps`;
    return this.http.post(API_URL, data)
  }

  //anouncement

  getAllMalnourished(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/poverty-get-all-data-malnourished`);
  }

  getAllSenior(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/poverty-get-all-data-senior`);
  }


  getAllpregnant(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/poverty-get-all-data-pregnant`);
  }




  createInventoryMedic(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}save-medication-info`;
    return this.http.post(API_URL, data)
  }
  //admin

  getAllAdminUser(data: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/admin-user-get-all-data` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }

  ViewSingleAdminInfo(id: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/admin-user-get-single-data` +  `?id=${id}`);
  }

  DeleteSingleAdminInfo(id: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/admin-user-delete-single-data` +  `?id=${id}`);
  }

  CreateAdminUser(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}admin-user-create-data`;
    return this.http.post(API_URL, data)
  }

  UpdateAdminUser(data: any, id){
    var formData = new FormData();
    for(let key in data){
      formData.append(key, data[key])
    }

    // console.log("from admin service", id, formData)

    return this.http.patch(`${this.HTTP_API_URL_PRIVATE}/user-update-single-data` + `?id=${id}`, formData,);
  }



  //end admin




  createFollowCheckup(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}add-schedule-pregnant`;
    return this.http.post(API_URL, data)
  }


  createUserInfo(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}user-post-data`;
    return this.http.post(API_URL, data)
  }


  getSingleUniqueCode(data: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-single-unique-code` +  `?unique_code=${data}`);
  }

  getAllUserInfo(data: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-all-data` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }

  getAllChildStatusHistory(data: any, id): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-archived-child-status-history` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}&id=${id}`);
  }

  getAllWomensPeriod(data: any, id): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-archived-women-period-history` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}&id=${id}`);
  }

  getAllCountChildStatus(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-all-child-count`);
  }
  getAllCountPopulationStatus(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-all-population-count`);
  }
  getAllCountMaleZone(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-all-male-zone`);
  }
  getAllCountFemaleZone(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-all-female-zone`);
  }
  getAllCountPregnant(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-all-pregnant`);
  }
  getAllCountSenior(): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-all-senior`);
  }




  ViewSingleUserInfo(id: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-single-data` +  `?id=${id}`);
  }

  DeleteSingleUserInfo(id: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-delete-single-data` +  `?id=${id}`);
  }

  updateSingleUserInfo(data: any, id){
    var formData = new FormData();
    for(let key in data){
      formData.append(key, data[key])
    }

    // console.log("from admin service", id, formData)

    return this.http.patch(`${this.HTTP_API_URL_PRIVATE}/user-update-single-data` + `?id=${id}`, formData,);
  }

  updateChildStatus(data: any, id){
    var formData = new FormData();
    for(let key in data){
      formData.append(key, data[key])
    }

    // console.log("from admin service", id, formData)

    return this.http.patch(`${this.HTTP_API_URL_PRIVATE}/user-archived-child-status` + `?id=${id}`, formData,);
  }






  //poverty
  createPoverty(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}poverty-post-data`;
    return this.http.post(API_URL, data)
  }

  getAllPovertyAccount(data: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/poverty-get-all-data` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }


  ViewSinglePoverty(id: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/poverty-get-single-data` +  `?id=${id}`);
  }


  deleteSinglePoverty(id: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/poverty-delete-single-data` +  `?id=${id}`);
  }

  updateSingleUserPoverty(data: any, id){
    var formData = new FormData();
    for(let key in data){
      formData.append(key, data[key])
    }

    // console.log("from admin service", id, formData)

    return this.http.patch(`${this.HTTP_API_URL_PRIVATE}/poverty-update-single-data` + `?id=${id}`, formData,);
  }

  //Medicine
  createMedicine(data: any): Observable<any>{
    let API_URL = `${this.HTTP_API_URL_PRIVATE}medicine-post-data`;
    return this.http.post(API_URL, data)
  }


  getAllMedicineInfo(data: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/medicine-get-all-data` +  `?page=${data.page}&size=${data.pageSize}&searchString=${data.searchString}`);
  }

  getAllMeds(){
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/meds-get-all-data`);
  }

  //  updateSingleMedicine(data: any): Observable<any>{
  //   let API_URL = `${this.HTTP_API_URL_PRIVATE}medicine-post-data`;
  //   return this.http.post(API_URL, data)
  // }

  updateSingleMedicine(data: any, id){
    var formData = new FormData();
    for(let key in data){
      formData.append(key, data[key])
    }

    // console.log("from admin service", id, formData)

    return this.http.patch(`${this.HTTP_API_URL_PRIVATE}/medicine-update-single-data` + `?id=${id}`, formData,);
  }

  getSingleMedicine(id:any){
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/medicine-get-single-data` +  `?id=${id}`);
  }


  //


  //medication
  getSingleMedication(id:any){
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/medication-get-single-data` +  `?id=${id}`);
  }


  DeleteSingleMedicine(id: any): Observable<any>{
    return this.http.delete(`${this.HTTP_API_URL_PRIVATE}/medicine-delete-single-data` +  `?id=${id}`);
  }

  ViewSingleMedicine(id: any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/medicine-get-single-data` +  `?id=${id}`);
  }


  //REPORTS
  getAllUsersReport(days:any, years :any, months:any, zones:any, person_status:any, childStatusSearchString:any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/user-get-all-report`
     +  `?days=${days}&years=${years}&months=${months}&zones=${zones}&person_status=${person_status}&childStatusSearchString=${childStatusSearchString}`);
  }

  getAllPovertyReport(zones:any): Observable<any>{
    return this.http.get(`${this.HTTP_API_URL_PRIVATE}/poverty-get-all-report`
     +  `?zones=${zones}`);
  }


  //deceased
  updateDeceasedUser(data: any, id){
    var formData = new FormData();
    for(let key in data){
      formData.append(key, data[key])
    }

    // console.log("from admin service", id, formData)

    return this.http.patch(`${this.HTTP_API_URL_PRIVATE}/user-archived-deceased` + `?id=${id}`, formData,);
  }





}
