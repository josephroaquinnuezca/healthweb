import { MatDialog } from '@angular/material/dialog';
import { UpdateMedicineDialogComponent } from './../medicine-info/update-medicine-dialog/update-medicine-dialog.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ViewMedicineDialogComponent } from '../medicine-info/view-medicine-dialog/view-medicine-dialog.component';
import { DeleteMedicineDialogComponent } from '../medicine-info/delete-medicine-dialog/delete-medicine-dialog.component';
import { AddMedicineDialogComponent } from '../medicine-info/add-medicine-dialog/add-medicine-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { AdminserviceService } from '../../service/adminservice.service';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-inventory-dashboard',
  templateUrl: './inventory-dashboard.component.html',
  styleUrls: ['./inventory-dashboard.component.scss']
})
export class InventoryDashboardComponent implements OnInit {



  @ViewChild('paginator', { static: false}) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort


  //mat table
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['no', 'bacthno', 'prodname', 'gname', 'dosage', 'medtype', 'quantity', 'manufacturer',   ];
  // dataSource = ELEMENT_DATA;


   //search
   pageOptionSize: any = [5, 10, 20];
   paginate: any = {
    totalCount!: 1100,
    pageSize: 10,
    page: 0,
    searchString: ''
  }

  //
  getAllData: any = [];


  constructor(
    private getalldata: AdminserviceService,
    public dialog: MatDialog) {}

    ngOnInit(): void {
      this.getAllMedicineInfo();
    }

    //search
    onChangedPage(page: PageEvent) {
      console.log("PAGE EVENT", page)
      this.paginate.page = page.pageIndex
      this.paginate.pageSize = page.pageSize
      // console.log("PAGE LANG", page)
      this.getAllMedicineInfo();

    }
    // end

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim();
      filterValue = filterValue.toLowerCase();
      this.paginate.searchString = filterValue;
      console.log("view search string", filterValue)

      this.paginator.pageIndex == 0
        ? this.getAllMedicineInfo()
        : this.paginator.firstPage();
    }

    getAllMedicineInfo() {
      this.getalldata
        .getAllMedicineInfo(this.paginate)
        .subscribe(async(data) => {
          console.log(data);

          var result: any = await data

          console.log("getAllPovertyAccount", result)

          this.getAllData = result.data;

          console.log("Data", this.getAllData.data)
          this.paginate.totalCount = result.data.totalCount
          this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
          this.dataSource.data = this.getAllData.data;

          // this.dataSourceAll.data = this.getAllData;
        });
    }




  addmedicineinfo(){
    const dialogRef = this.dialog.open(AddMedicineDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  //delete user
  deleteMedicineInfo(){
    const dialogRef = this.dialog.open(DeleteMedicineDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


  //edit User
  editMedicineInfo(){
    const dialogRef = this.dialog.open(UpdateMedicineDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  //view
  viewMedicineInfo(){
    const dialogRef = this.dialog.open(ViewMedicineDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
