import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

@Component({
  selector: 'app-view-poverty-dialog',
  templateUrl: './view-poverty-dialog.component.html',
  styleUrls: ['./view-poverty-dialog.component.scss']
})
export class ViewPovertyDialogComponent implements OnInit {


  dialogData:any;

  DataValues:any;

  constructor(
    // private clientService: ClientServiceService,
    private getalldata: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }


  ngOnInit(): void {

    this.getalldata.ViewSinglePoverty(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data

      console.log("results", result)

      // this.getAllClient();
    });

    console.log(this.dialogData.id)
  }




}
