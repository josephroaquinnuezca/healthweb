import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePovertyDialogComponent } from './delete-poverty-dialog.component';

describe('DeletePovertyDialogComponent', () => {
  let component: DeletePovertyDialogComponent;
  let fixture: ComponentFixture<DeletePovertyDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletePovertyDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePovertyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
