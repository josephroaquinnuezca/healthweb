import { AdminserviceService } from './../../../service/adminservice.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-poverty-dialog',
  templateUrl: './delete-poverty-dialog.component.html',
  styleUrls: ['./delete-poverty-dialog.component.scss']
})
export class DeletePovertyDialogComponent implements OnInit {


  dialogData:any;

  DataValues:any;

  constructor(
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<DeletePovertyDialogComponent>,
    private getalldata: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }


  ngOnInit(): void {


    console.log("Error:", this.dialogData.id)


    console.log(this.dialogData.id)
  }

  onSubmit(){



    this.getalldata.deleteSinglePoverty(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data

      console.log("results", result)

      this.dialogRef.close();

      // this.getAllClient();
    });
  }
}
