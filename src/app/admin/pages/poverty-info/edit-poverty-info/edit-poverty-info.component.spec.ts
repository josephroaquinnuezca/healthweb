import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPovertyInfoComponent } from './edit-poverty-info.component';

describe('EditPovertyInfoComponent', () => {
  let component: EditPovertyInfoComponent;
  let fixture: ComponentFixture<EditPovertyInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPovertyInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPovertyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
