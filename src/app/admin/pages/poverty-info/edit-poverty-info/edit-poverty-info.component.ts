import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';



interface Gender {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-edit-poverty-info',
  templateUrl: './edit-poverty-info.component.html',
  styleUrls: ['./edit-poverty-info.component.scss']
})

export class EditPovertyInfoComponent implements OnInit {

  gadgets: string[] = ['TV', 'Radio', 'Washing Machine', 'Mobile Phone', 'Computer', 'None'];
  water: string[] = ['Own Use', 'Fauset', 'Deep Well'];

  house: string[] = ['Concrete', 'Nipa House'];

  dialogData:any;



   //add
   formAddUser: FormGroup


   @Output()
   dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();

   foods: Gender[] = [
     {value: 'Male', viewValue: 'Male'},
     {value: 'Female', viewValue: 'Female'},
   ];

   constructor(
    public UserInfoService: AdminserviceService,
    public dialogRef: MatDialogRef<EditPovertyInfoComponent>,
    private toastr: ToastrService,
     private formgroup: FormBuilder,
     private spinner: NgxSpinnerService,
     @Inject(MAT_DIALOG_DATA) public results,
   ) {
    this.dialogData = results
   }

   ngOnInit(): void {
     this.AddUserFormValidation() //declare the functions that is created
      this.ViewSingleData()
    //  this.spinner.show();

    //  setTimeout(() => {
    //    /** spinner ends after 5 seconds */
    //    this.spinner.hide();
    //  }, 5000);
   }

   //
   get f() {
     return this.formAddUser.controls;
   }

   //validation of form
   AddUserFormValidation() {
     this.formAddUser = this.formgroup.group({
       fullname: ['', Validators.required],
       cell_no: ['', [ Validators.required, Validators.pattern('^[0-9]*$'),
       Validators.minLength(11),
       Validators.maxLength(11)]],
       mincome: ['', Validators.required],
       occupation: ['', Validators.required],
       studying: [''],
       pwd: [''],
       pwdMember: [''],
       member: ['', Validators.required],
       working: ['', Validators.required],
       children: ['', Validators.required],
       fourps: ['', Validators.required],
       gadgets: ['', Validators.required],
       water: ['', Validators.required],
       house: ['', Validators.required],


     });
   }

   //submit button execution

   onSubmit() {

     if (this.formAddUser.invalid) {
       //this is to show all the required fields

       this.formAddUser.markAllAsTouched();
       this.toastr.error('Please complete all required fields');
       return;
     } else {


      const data: any = {

        fullname: this.formAddUser.get('fullname').value,
        cell_no: this.formAddUser.get('cell_no').value,
        mincome: this.formAddUser.get('mincome').value,
        occupation:  this.formAddUser.get('occupation').value,
        studying:  this.formAddUser.get('studying').value,
        pwd: this.formAddUser.get('pwd').value,
        pwdMember: this.formAddUser.get('pwdMember').value,
        member: this.formAddUser.get('member').value,
        working: this.formAddUser.get('working').value,
        children: this.formAddUser.get('children').value,
        fourps: this.formAddUser.get('fourps').value,
        gadgets: this.formAddUser.get('gadgets').value,
        water: this.formAddUser.get('water').value,
        house: this.formAddUser.get('house').value,


      }

      const stringify = JSON.stringify(data);
      console.log("With Stringify :" , stringify);


      console.log(data)


      this.UserInfoService.updateSingleUserPoverty(data, this.dialogData.id).subscribe(data =>{
            console.log(data);
            this.toastr.success('Successfully Inserted');
            this.dialogRef.close();
            // this.spinner.show();

            // setTimeout(() => {
            //   /** spinner ends after 5 seconds */
            //   this.dialogRef.close();
            //   this.spinner.hide();
            // }, 5000);
            // this.router.navigate(['/login']);
          }, err => {
            if (err instanceof HttpErrorResponse) {
              console.log(err.status)
              console.log("hello",err.error.description)
              this.toastr.error(err.error.description);
            }else{
              console.log(err)
            }

          }, () => {
            console.log('request completed')
          });


       // const username = this.formlogin.get('username').value;
       // const password = this.formlogin.get('password').value;

       // console.log('LOGIN VALUE', username, password);


     }
   }//ends

   ViewSingleData(){

    const id =  this.dialogData.id



    this.UserInfoService.ViewSinglePoverty(id).subscribe(data =>{
      console.log("Get Single data", data);


      const dataform: any = data

      // fullname: this.formAddUser.get('fullname').value,
      //   mincome: this.formAddUser.get('mincome').value,
      //   occupation:  this.formAddUser.get('occupation').value,
      //   studying:  this.formAddUser.get('studying').value,
      //   pwd: this.formAddUser.get('pwd').value,
      //   member: this.formAddUser.get('member').value,
      //   fourps: this.formAddUser.get('fourps').value,
      //   gadgets: this.formAddUser.get('gadgets').value,
      //   water: this.formAddUser.get('water').value,
      //   house: this.formAddUser.get('house').value,


      this.formAddUser.get("fullname").patchValue(dataform?.data.fullname);
      this.formAddUser.get("cell_no").patchValue(dataform?.data.cell_no);
      this.formAddUser.get("mincome").patchValue(dataform?.data.mincome);
      this.formAddUser.get("occupation").patchValue(dataform?.data.occupation);
      this.formAddUser.get("studying").patchValue(dataform?.data.studying);
      this.formAddUser.get("pwd").patchValue(dataform?.data.pwd);
      this.formAddUser.get("pwdMember").patchValue(dataform?.data.pwdMember);
      this.formAddUser.get("member").patchValue(dataform?.data.member);
      this.formAddUser.get("working").patchValue(dataform?.data.working);
      this.formAddUser.get("children").patchValue(dataform?.data.children);
      this.formAddUser.get("fourps").patchValue(dataform?.data.fourps);



    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });


  }

}
