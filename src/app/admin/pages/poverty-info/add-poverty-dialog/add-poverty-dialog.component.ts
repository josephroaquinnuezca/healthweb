import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef } from '@angular/material/dialog';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

interface Gender {
  value: string;
  viewValue: string;
}


interface Zone {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-add-poverty-dialog',
  templateUrl: './add-poverty-dialog.component.html',
  styleUrls: ['./add-poverty-dialog.component.scss']
})
export class AddPovertyDialogComponent implements OnInit {

  zone: Zone[] = [
    { value: '1', viewValue: 'Zone 1' },
    { value: '2', viewValue: 'Zone 2' },
    { value: '3', viewValue: 'Zone 3' },
    { value: '4', viewValue: 'Zone 4' },
    { value: '5', viewValue: 'Zone 5' },
    { value: '6', viewValue: 'Zone 6' },
    { value: '7', viewValue: 'Zone 7' },
  ];

  gadgets: string[] = ['TV', 'Radio', 'Washing Machine', 'Mobile Phone', 'Computer', 'None'];
  water: string[] = ['Own Use', 'Fauset', 'Deep Well'];

  house: string[] = ['Concrete', 'Nipa House'];

   //add
   formAddUser: FormGroup


   @Output()
   dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();

   foods: Gender[] = [
     {value: 'Male', viewValue: 'Male'},
     {value: 'Female', viewValue: 'Female'},
   ];

   constructor(
    public UserInfoService: AdminserviceService,
    public dialogRef: MatDialogRef<AddPovertyDialogComponent>,
    private toastr: ToastrService,
     private formgroup: FormBuilder,
     private spinner: NgxSpinnerService,
   ) { }

   ngOnInit(): void {
     this.AddUserFormValidation() //declare the functions that is created

    //  this.spinner.show();

    //  setTimeout(() => {
    //    /** spinner ends after 5 seconds */
    //    this.spinner.hide();
    //  }, 5000);
   }

   //
   get f() {
     return this.formAddUser.controls;
   }

   //validation of form
   AddUserFormValidation() {
     this.formAddUser = this.formgroup.group({
       fullname: ['', Validators.required],
       mincome: ['', Validators.required],
       cell_no: ['', [ Validators.required, Validators.pattern('^[0-9]*$'),
       Validators.minLength(11),
       Validators.maxLength(11)]],
       zone: ['', Validators.required],
       occupation: ['', Validators.required],
       studying: [''],
       pwd: [''],
       pwdMember: [''],
       member: ['', Validators.required],
       fourps: ['', Validators.required],
       gadgets: ['', Validators.required],
       water: ['', Validators.required],
       house: ['', Validators.required],


     });
   }

   //submit button execution

   onSubmit() {

     if (this.formAddUser.invalid) {
       //this is to show all the required fields

       this.formAddUser.markAllAsTouched();
       this.toastr.error('Please complete all required fields');
       return;
     } else {


      const data: any = {

        fullname: this.formAddUser.get('fullname').value,
        cell_no: this.formAddUser.get('cell_no').value,
        zone: this.formAddUser.get('zone').value,
        mincome: this.formAddUser.get('mincome').value,
        occupation:  this.formAddUser.get('occupation').value,
        studying:  this.formAddUser.get('studying').value,
        pwd: this.formAddUser.get('pwd').value,
        pwdMember: this.formAddUser.get('pwdMember').value,
        member: this.formAddUser.get('member').value,
        fourps: this.formAddUser.get('fourps').value,
        gadgets: this.formAddUser.get('gadgets').value,
        water: this.formAddUser.get('water').value,
        house: this.formAddUser.get('house').value,


      }

      const stringify = JSON.stringify(data);
      console.log("With Stringify :" , stringify);


      console.log(data)


      this.UserInfoService.createPoverty(data).subscribe(data =>{
            console.log(data);
            this.toastr.success('Successfully Inserted');
            this.dialogRef.close();
            // this.spinner.show();

            // setTimeout(() => {
            //   /** spinner ends after 5 seconds */
            //   this.dialogRef.close();
            //   this.spinner.hide();
            // }, 5000);
            // this.router.navigate(['/login']);
          }, err => {
            if (err instanceof HttpErrorResponse) {
              console.log(err.status)
              console.log("hello",err.error.description)
              this.toastr.error(err.error.description);
            }else{
              console.log(err)
            }

          }, () => {
            console.log('request completed')
          });


       // const username = this.formlogin.get('username').value;
       // const password = this.formlogin.get('password').value;

       // console.log('LOGIN VALUE', username, password);


     }
   }//ends


}
