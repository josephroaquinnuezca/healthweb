import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

interface role {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-register-account',
  templateUrl: './register-account.component.html',
  styleUrls: ['./register-account.component.scss'],
})
export class RegisterAccountComponent implements OnInit {
  Role: role[] = [
    { value: '0', viewValue: 'Admin' },
    { value: '1', viewValue: 'Secretary' },
    { value: '2', viewValue: 'BHW' },
    { value: '3', viewValue: 'Household' },
  ];

  //add
  adduser: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<RegisterAccountComponent>,
    public addUserData: AdminserviceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder // private spinner: NgxSpinnerService not required
  ) {}

  ngOnInit(): void {
    this.AddUserFormValidation(); //declare the functions that is created below
    // this.bmiFunction()
  }

  //
  get f() {
    return this.adduser.controls;
  }

  //validation of form
  AddUserFormValidation() {
    this.adduser = this.formgroup.group({
      name: ['', Validators.required],
      mail_address: ['', Validators.required],
      password: ['', Validators.required],
      retypepassword: ['', Validators.required],
      user_role: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.adduser.invalid) {
      this.adduser.markAllAsTouched();
      this.toastr.error('Please complete all required fields');

      return;
    } else {
      const data = {
        name: this.adduser.get('name').value,
        mail_address: this.adduser.get('mail_address').value,
        password: this.adduser.get('password').value,
        user_role: this.adduser.get('user_role').value,
      };

      const stringify = JSON.stringify(data);
      console.log('With Stringify :', stringify);

      console.log(data);

      this.addUserData.CreateAdminUser(data).subscribe(
        (data) => {
          console.log(data);

          this.dialogRef.close();

          this.toastr.success('Successfully Inserted');
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  } //end
}
