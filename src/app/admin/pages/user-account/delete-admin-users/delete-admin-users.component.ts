import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

@Component({
  selector: 'app-delete-admin-users',
  templateUrl: './delete-admin-users.component.html',
  styleUrls: ['./delete-admin-users.component.scss']
})
export class DeleteAdminUsersComponent implements OnInit {

  dialogData:any;

  DataValues:any;

  constructor(
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<DeleteAdminUsersComponent>,
    private deleteSingleData: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }

  ngOnInit(): void {
  }

  onSubmit(){



    this.deleteSingleData.DeleteSingleAdminInfo(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data

      console.log("results", result)

      this.dialogRef.close();

      // this.getAllClient();
    });
  }

}
