import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAdminUsersComponent } from './delete-admin-users.component';

describe('DeleteAdminUsersComponent', () => {
  let component: DeleteAdminUsersComponent;
  let fixture: ComponentFixture<DeleteAdminUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteAdminUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAdminUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
