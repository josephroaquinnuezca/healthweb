import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

@Component({
  selector: 'app-view-admin-users',
  templateUrl: './view-admin-users.component.html',
  styleUrls: ['./view-admin-users.component.scss']
})
export class ViewAdminUsersComponent implements OnInit {

  dialogData: any;

  DataValues: any;

  constructor(
    // private clientService: ClientServiceService,
    private getSingledata: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
    public dialog: MatDialog
  ) {
    this.dialogData = results;
  }

  ngOnInit(): void {

    this.fetchSingleData();



  }


  fetchSingleData() {
    this.getSingledata.ViewSingleAdminInfo(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data;

      console.log('results', result);

      // this.getAllClient();
    });
  }

}
