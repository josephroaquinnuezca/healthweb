import { AddMedicineDialogComponent } from './../medicine-info/add-medicine-dialog/add-medicine-dialog.component';
import { MatSort } from '@angular/material/sort';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AdminserviceService } from '../../service/adminservice.service';
import { RegisterAccountComponent } from './register-account/register-account.component';
import { DeleteMedicineDialogComponent } from '../medicine-info/delete-medicine-dialog/delete-medicine-dialog.component';
import { UpdateMedicineDialogComponent } from '../medicine-info/update-medicine-dialog/update-medicine-dialog.component';
import { ViewMedicineDialogComponent } from '../medicine-info/view-medicine-dialog/view-medicine-dialog.component';
import { DeleteAdminUsersComponent } from './delete-admin-users/delete-admin-users.component';
import { ViewAdminUsersComponent } from './view-admin-users/view-admin-users.component';
import { EditAdminUsersComponent } from './edit-admin-users/edit-admin-users.component';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {




  @ViewChild('paginator', { static: false}) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort


  //mat table
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['no', 'bacthno', 'gname', 'quantity', 'manufacturer',   'controls'];
  // dataSource = ELEMENT_DATA;


   //search
   pageOptionSize: any = [5, 10, 20];
   paginate: any = {
    totalCount!: 1100,
    pageSize: 10,
    page: 0,
    searchString: ''
  }

  //
  getAllData: any = [];


  constructor(
    private getalldata: AdminserviceService,
    public dialog: MatDialog) {}

    ngOnInit(): void {
      this.getAllAdminUser();
    }

    //search
    onChangedPage(page: PageEvent) {
      console.log("PAGE EVENT", page)
      this.paginate.page = page.pageIndex
      this.paginate.pageSize = page.pageSize
      // console.log("PAGE LANG", page)
      this.getAllAdminUser();

    }
    // end

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim();
      filterValue = filterValue.toLowerCase();
      this.paginate.searchString = filterValue;
      console.log("view search string", filterValue)

      this.paginator.pageIndex == 0
        ? this.getAllAdminUser()
        : this.paginator.firstPage();
    }

    getAllAdminUser() {
      this.getalldata
        .getAllAdminUser(this.paginate)
        .subscribe(async(data) => {
          console.log(data);

          var result: any = await data

          console.log("Medicine", result)

          this.getAllData = result.data;

          console.log("Data", this.getAllData.data)
          this.paginate.totalCount = result.data.totalCount
          this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
          this.dataSource.data = this.getAllData.data;

          // this.dataSourceAll.data = this.getAllData;
        });
    }




  addmedicineinfo(){
    const dialogRef = this.dialog.open(RegisterAccountComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getAllAdminUser();
      console.log(`Dialog result: ${result}`);
    });
  }

  //delete user
  deleteSingleAdmin(id){
    const dialogRef = this.dialog.open(DeleteAdminUsersComponent, {
      data:{
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllAdminUser();
      console.log(`Dialog result: ${result}`);
    });
  }


  //edit User
  editMedicineInfo(id){
    const dialogRef = this.dialog.open(EditAdminUsersComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllAdminUser();
      console.log(`Dialog result: ${result}`);
    });
  }

  //view
  viewMedicineInfo(id){
    const dialogRef = this.dialog.open(ViewAdminUsersComponent, {
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
