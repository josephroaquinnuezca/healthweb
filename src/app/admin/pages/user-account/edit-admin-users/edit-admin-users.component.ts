import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

interface role {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-edit-admin-users',
  templateUrl: './edit-admin-users.component.html',
  styleUrls: ['./edit-admin-users.component.scss']
})
export class EditAdminUsersComponent implements OnInit {

  Role: role[] = [
    { value: '0', viewValue: 'Admin' },
    { value: '1', viewValue: 'Secretary' },
    { value: '2', viewValue: 'BHW' },
    { value: '3', viewValue: 'Household' },
  ];

  //add
  adduser: FormGroup;

  dialogData:any;


  constructor(
    public dialogRef: MatDialogRef<EditAdminUsersComponent>,
    public addUserData: AdminserviceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,// private spinner: NgxSpinnerService not required
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }

  ngOnInit(): void {
    this.AddUserFormValidation(); //declare the functions that is created below
    this.ViewSingleData();
  }

  //
  get f() {
    return this.adduser.controls;
  }

  //validation of form
  AddUserFormValidation() {
    this.adduser = this.formgroup.group({
      name: ['', Validators.required],
      mail_address: ['', Validators.required],
      password: ['', Validators.required],
      retypepassword: ['', Validators.required],
      user_role: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.adduser.invalid) {
      this.adduser.markAllAsTouched();
      this.toastr.error('Please complete all required fields');

      return;
    } else {
      const data = {
        name: this.adduser.get('name').value,
        mail_address: this.adduser.get('mail_address').value,
        password: this.adduser.get('password').value,
        user_role: this.adduser.get('user_role').value,
      };

      const stringify = JSON.stringify(data);
      console.log('With Stringify :', stringify);

      console.log(data);

      this.addUserData.UpdateAdminUser(data, this.dialogData.id).subscribe(
        (data) => {
          console.log(data);

          this.dialogRef.close();

          this.toastr.success('Successfully Inserted');
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  } //

  ViewSingleData(){

    const id =  this.dialogData.id



    this.addUserData.ViewSingleAdminInfo(id).subscribe(data =>{
      console.log("Get Single data", data);


      const dataform: any = data


      this.adduser.get("name").patchValue(dataform?.data.name);
      this.adduser.get("mail_address").patchValue(dataform?.data.mail_address);
      this.adduser.get("user_role").patchValue(dataform?.data.user_role);


    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });


  }

}
