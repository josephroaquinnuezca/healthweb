import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';


@Component({
  selector: 'app-add-medicine-dialog',
  templateUrl: './add-medicine-dialog.component.html',
  styleUrls: ['./add-medicine-dialog.component.scss']
})
export class AddMedicineDialogComponent implements OnInit {


  medicine_type = [
    { value: 'Liquid', viewValue: 'Liquid' },
    { value: 'Tablet', viewValue: 'Tablet' },
    { value: 'Capsules', viewValue: 'Capsules' },
    { value: 'Suppositories', viewValue: 'Suppositories' },
    { value: 'Drops', viewValue: 'Drops' },
    { value: 'Injections', viewValue: 'Injections' },
  ];

  unit = [
    { value: 'Box', viewValue: 'Box' },
    { value: 'Bottle', viewValue: 'Bottle' },
    { value: 'Pads', viewValue: 'Pads' },
  ];

  mindate = new Date();

  expirydate = new Date();

   //add
   medicineform: FormGroup


   constructor(
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AddMedicineDialogComponent>,
    public MedicineService: AdminserviceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
  ) { }


  ngOnInit(): void {
    this.AddUserFormValidation();



  }

  //
  get f() {
    return this.medicineform.controls;
  }

  //validation of form
  AddUserFormValidation() {
    this.medicineform = this.formgroup.group({
      bacthno: ['', Validators.required],
      prodname: ['', Validators.required],
      gname: ['', Validators.required],
      dosage: ['', Validators.required],
      medtype: [''],
      manufacturer: ['', Validators.required],
      manufacturingdate: ['', Validators.required],
      exdate: ['', Validators.required],
      quantity: ['', Validators.required],
      unit: [''],

    });
  }
  //
  onSubmit() {

    if (this.medicineform.invalid) {
      //this is to show all the required fields

      this.medicineform.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      return;
    } else {


      const startdate = this.medicineform.get('manufacturingdate').value
      const enddate = this.medicineform.get('exdate').value


      this.expirydate.setDate(enddate.getDate() + 1);


        // const diffTime =   enddate.getTime() - startdate.getTime();
        // const resultdata = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        // console.log("remaining days", resultdata)





     const data: any = {

       bacthno: this.medicineform.get('bacthno').value,
       prodname: this.medicineform.get('prodname').value,
       gname:  this.medicineform.get('gname').value,
       dosage: this.medicineform.get('dosage').value,
       medtype:  this.medicineform.get('medtype').value ,
       manufacturer:  this.medicineform.get('manufacturer').value,
       manufacturingdate:  this.medicineform.get('manufacturingdate').value,
       exdate:  this.medicineform.get('exdate').value,
       quantity: this.medicineform.get('quantity').value,
       unit: this.medicineform.get('unit').value,



     }

     const stringify = JSON.stringify(data);
     console.log("With Stringify :" , stringify);


     console.log(data)


     this.MedicineService.createMedicine(data).subscribe(data =>{
           console.log(data);
           this.toastr.success('Successfully Inserted');
           this.dialogRef.close();
         }, err => {
           if (err instanceof HttpErrorResponse) {
             console.log(err.status)
             console.log("hello",err.error.description)
             this.toastr.error(err.error.description);
           }else{
             console.log(err)
           }

         }, () => {
           console.log('request completed')
         });



    }
  }//ends

  //validation
  allowNumericDigitsOnlyOnKeyUp(e) {
		const charCode = e.which ? e.which : e.keyCode;

		if (charCode > 31 && (charCode < 48 || charCode > 57)) {

      this.snackBar.open("OOPs! Only numeric values or digits allowed", null, {
        duration: 2000,
      });
			// this.msg = '<span class="msg">OOPs! Only numeric values or digits allowed</span>';
		}
	}

}
