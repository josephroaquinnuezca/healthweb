import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminserviceService } from '../../service/adminservice.service';
import { AddUserDialogComponent } from '../user-info/add-user-dialog/add-user-dialog.component';
import { DeleteUserDialogComponent } from '../user-info/delete-user-dialog/delete-user-dialog.component';
import { EditUserDialogComponent } from '../user-info/edit-user-dialog/edit-user-dialog.component';
import { AddMedicineDialogComponent } from './add-medicine-dialog/add-medicine-dialog.component';
import { DeleteMedicineDialogComponent } from './delete-medicine-dialog/delete-medicine-dialog.component';
import { UpdateMedicineDialogComponent } from './update-medicine-dialog/update-medicine-dialog.component';
import { ViewMedicineDialogComponent } from './view-medicine-dialog/view-medicine-dialog.component';
import * as moment from 'moment';



@Component({
  selector: 'app-medicine-info',
  templateUrl: './medicine-info.component.html',
  styleUrls: ['./medicine-info.component.scss']
})
export class MedicineInfoComponent implements OnInit {




  @ViewChild('paginator', { static: false}) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort


  //mat table
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['no', 'bacthno', 'gname', 'quantity', 'manufacturer', 'expiration', 'status_date',  'controls'];
  // dataSource = ELEMENT_DATA;


   //search
   pageOptionSize: any = [5, 10, 20];
   paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: ''
  }

  //
  getAllData: any = [];

  today = moment(new Date()).format();


  constructor(
    private getalldata: AdminserviceService,
    public dialog: MatDialog) {}

    ngOnInit(): void {
      this.getAllMedicineInfo();




    }

    //search
    onChangedPage(page: PageEvent) {
      console.log("PAGE EVENT", page)
      this.paginate.page = page.pageIndex
      this.paginate.pageSize = page.pageSize
      // console.log("PAGE LANG", page)
      this.getAllMedicineInfo();

    }
    // end

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim();
      filterValue = filterValue.toLowerCase();
      this.paginate.searchString = filterValue;
      console.log("view search string", filterValue)

      this.paginator.pageIndex == 0
        ? this.getAllMedicineInfo()
        : this.paginator.firstPage();
    }

    getAllMedicineInfo() {
      this.getalldata
        .getAllMedicineInfo(this.paginate)
        .subscribe(async(data) => {
          console.log(data);

          var result: any = await data

         

          console.log("Medicine", result)

          // console.log("expiration date", this.getAllData.data.manufacturingdate)

       

          this.getAllData = result.data;

          ;

          console.log("Data", this.getAllData.data)
          this.paginate.totalCount = result.data.totalCount
          this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
          this.dataSource.data = this.getAllData.data;

          // this.dataSourceAll.data = this.getAllData;
        });
    }




  addmedicineinfo(){
    const dialogRef = this.dialog.open(AddMedicineDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getAllMedicineInfo();
      console.log(`Dialog result: ${result}`);
    });
  }

  //delete user
  deleteMedicineInfo(id){
    const dialogRef = this.dialog.open(DeleteMedicineDialogComponent, {
      data:{
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllMedicineInfo();
      console.log(`Dialog result: ${result}`);
    });
  }


  //edit User
  editMedicineInfo(id){
    const dialogRef = this.dialog.open(UpdateMedicineDialogComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllMedicineInfo();
      console.log(`Dialog result: ${result}`);
    });
  }

  //view
  viewMedicineInfo(id){
    const dialogRef = this.dialog.open(ViewMedicineDialogComponent, {
      data: {
        id: id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
