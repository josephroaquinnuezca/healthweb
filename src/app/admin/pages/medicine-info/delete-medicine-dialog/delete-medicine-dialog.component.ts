import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

@Component({
  selector: 'app-delete-medicine-dialog',
  templateUrl: './delete-medicine-dialog.component.html',
  styleUrls: ['./delete-medicine-dialog.component.scss']
})
export class DeleteMedicineDialogComponent implements OnInit {

  
  dialogData:any;

  DataValues:any;

  constructor(
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<DeleteMedicineDialogComponent>,
    private getalldata: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }


  ngOnInit(): void {



    console.log(this.dialogData.id)
  }

  onSubmit(){



    this.getalldata.DeleteSingleMedicine(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data

      console.log("results", result)

      this.dialogRef.close();

      // this.getAllClient();
    });
  }

}
