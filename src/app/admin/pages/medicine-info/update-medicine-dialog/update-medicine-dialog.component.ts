import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

@Component({
  selector: 'app-update-medicine-dialog',
  templateUrl: './update-medicine-dialog.component.html',
  styleUrls: ['./update-medicine-dialog.component.scss']
})
export class UpdateMedicineDialogComponent implements OnInit {

  //
   //add
   medicineform: FormGroup


   dialogData:any;



   constructor(
    public dialogRef: MatDialogRef<UpdateMedicineDialogComponent>,
    public MedicineService: AdminserviceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {

    this.dialogData = results
   }


  ngOnInit(): void {
    this.AddUserFormValidation();

    // this.dialogData.id

    console.log("id", this.dialogData.id)

    this.ViewSingleData();
  }

  //
  get f() {
    return this.medicineform.controls;
  }

  //validation of form
  AddUserFormValidation() {
    this.medicineform = this.formgroup.group({
      bacthno: [''],
      prodname: ['', Validators.required],
      gname: ['', Validators.required],
      dosage: ['', Validators.required],
      medtype: [''],
      manufacturer: ['', Validators.required],
      manufacturingdate: ['', Validators.required],
      exdate: ['', Validators.required],
      quantity: ['', Validators.required],
      unit: [''],

    });
  }

  ViewSingleData(){

    const id =  this.dialogData.id



    this.MedicineService.getSingleMedicine(id).subscribe(data =>{
      console.log("Get Single data", data);


      const dataform: any = data


      this.medicineform.get("bacthno").patchValue(dataform?.data.bacthno);

      this.medicineform.get("prodname").patchValue(dataform?.data.prodname);
      this.medicineform.get("gname").patchValue(dataform?.data.gname);
      this.medicineform.get("dosage").patchValue(dataform?.data.dosage);
      this.medicineform.get("medtype").patchValue(dataform?.data.medtype);
      this.medicineform.get("manufacturer").patchValue(dataform?.data.manufacturer);
      this.medicineform.get("manufacturingdate").patchValue(dataform?.data.manufacturingdate);
      this.medicineform.get("exdate").patchValue(dataform?.data.exdate);
      this.medicineform.get("quantity").patchValue(dataform?.data.quantity);
      this.medicineform.get("unit").patchValue(dataform?.data.unit);




      // this.medicineform = this.formgroup.group({
      //   bacthno: [data?.bacthno],
      //   prodname: [''],
      //   gname: [''],
      //   dosage: [''],
      //   medtype: [''],
      //   manufacturer: [''],
      //   manufacturingdate: [''],
      //   exdate: [''],
      //   quantity: [''],

      // });

      // let contact = {
      //     bacthno: data.bacthno,
      //     prodname: data.prodname,
      //     gname:  data.gname,
      //     dosage:  data.dosage
      //     medtype:  data.medtype,
      //     manufacturer:  data.manufacturer,
      //     manufacturingdate:data.manufacturingdate,
      //     exdate: data.exdate,
      //     quantity:  data.quantity,
      //   }





    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });


  }

  //
  onSubmit() {

    if (this.medicineform.invalid) {
      //this is to show all the required fields

      this.medicineform.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      return;
    } else {


     const data: any = {

       bacthno: this.medicineform.get('bacthno').value,
       prodname: this.medicineform.get('prodname').value,
       gname:  this.medicineform.get('gname').value,
       dosage: this.medicineform.get('dosage').value,
       medtype:  this.medicineform.get('medtype').value ,
       manufacturer:  this.medicineform.get('manufacturer').value,
       manufacturingdate:  this.medicineform.get('manufacturingdate').value,
       exdate:  this.medicineform.get('exdate').value,
       quantity: this.medicineform.get('quantity').value,
       unit: this.medicineform.get('unit').value,



     }

     const stringify = JSON.stringify(data);
     console.log("With Stringify :" , stringify);


     console.log(data)


    const id =  this.dialogData.id



     this.MedicineService.updateSingleMedicine(data, id).subscribe(data =>{
           console.log(data);
           this.toastr.success('Successfully Inserted');

           setTimeout (() => {
                     this.dialogRef.close();
            this.toastr.success('Successfully Inserted');
                  }, 1000);




         }, err => {
           if (err instanceof HttpErrorResponse) {
             console.log(err.status)
             console.log("hello",err.error.description)
             this.toastr.error(err.error.description);
           }else{
             console.log(err)
           }

         }, () => {
           console.log('request completed')
         });



    }
  }//ends
}
