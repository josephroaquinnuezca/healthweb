import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PovertyReportPageComponent } from './poverty-report-page.component';

describe('PovertyReportPageComponent', () => {
  let component: PovertyReportPageComponent;
  let fixture: ComponentFixture<PovertyReportPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PovertyReportPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PovertyReportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
