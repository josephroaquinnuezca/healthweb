import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import * as moment from 'moment';

import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle,
} from 'node_modules/chart.js';


interface Zone {
  value: string;
  viewValue: string;
}

interface Date {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-poverty-report-page',
  templateUrl: './poverty-report-page.component.html',
  styleUrls: ['./poverty-report-page.component.scss']
})
export class PovertyReportPageComponent implements OnInit {

  usersreport: FormGroup;

  startYear = new Date().getFullYear() - 2;
  endYear = new Date().getFullYear() + 15;

  yearList = [];

  childstats = [
    { name: 'Normal', code: '01' },
    { name: 'Underweight', code: '02' },
    { name: 'Overweight', code: '03' },
  ];

  types = [
    { name: 'Child', code: '01' },
    { name: 'Pregnant', code: '02' },
    { name: 'Senior', code: '03' },

  ];

  months = [
    { name: 'Jan', code: '01' },
    { name: 'Feb', code: '02' },
    { name: 'Mar', code: '03' },
    { name: 'Apr', code: '04' },
    { name: 'May', code: '05' },
    { name: 'June', code: '06' },
    { name: 'Jul', code: '07' },
    { name: 'Aug', code: '08' },
    { name: 'Sep', code: '09' },
    { name: 'Oct', code: '10' },
    { name: 'Nov', code: '11' },
    { name: 'Dec', code: '12' },
  ];

  zones: Zone[] = [
    { value: '1', viewValue: 'Zone 1' },
    { value: '2', viewValue: 'Zone 2' },
    { value: '3', viewValue: 'Zone 3' },
    { value: '4', viewValue: 'Zone 4' },
    { value: '5', viewValue: 'Zone 5' },
    { value: '6', viewValue: 'Zone 6' },
    { value: '7', viewValue: 'Zone 7' },

  ];

  dates: Date[] = [
    { value: 'day', viewValue: 'Day' },
    { value: 'month', viewValue: 'Month' },
    { value: 'year', viewValue: 'Year' },
  ];

  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort;

  //mat table
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = [
    'no',
    'house',
    'social',
    '4ps',
    'pwd',
    'zone',
    'date'
  ];
  // dataSource = ELEMENT_DATA;

  //search
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
    monthSearchString: '',
    zoneSearchString: '',
    statusSearchString: '',
    childStatusSearchString: '',
  };

  //
  getAllData: any = [];

  fetchDataReport: any;

  day = true;
  month = true;
  year = true;

  child = true;


  showTableUser = false

  constructor(
    private getalldata: AdminserviceService,
    public dialog: MatDialog,
    private formgroup: FormBuilder,
    private toastr: ToastrService
  ) {
    Chart.register(
      ArcElement,
      LineElement,
      BarElement,
      PointElement,
      BarController,
      BubbleController,
      DoughnutController,
      LineController,
      PieController,
      PolarAreaController,
      RadarController,
      ScatterController,
      CategoryScale,
      LinearScale,
      LogarithmicScale,
      RadialLinearScale,
      TimeScale,
      TimeSeriesScale,
      Decimation,
      Filler,
      Legend,
      Title,
      Tooltip,
      SubTitle
    );
  }

  ngOnInit(): void {
    // this.fetchDataReport();
 //   this.pieChartFunction();
    this.getAllPovertyAccount();

    this.yearFunction();

    this.formValidation(); //form validation
  }

  //validation of form
  formValidation() {
    this.usersreport = this.formgroup.group({
      // dates: ['', Validators.required],
      // days: [''],
      // months: [''],
      // years: [''],
      zones: ['', Validators.required],
      // status: ['', Validators.required],
      // child_status: ['', Validators.required],
    });
  }

  yearFunction() {
    for (var i = this.startYear; i <= this.endYear; i++) {
      var obj = {};
      //set name for object array
      obj['name'] = i.toString();
      //set code for object array
      obj['code'] = i.toString();
      // insert into object array
      this.yearList.push(obj);
    }
  }

  selectStatus(event) {
    console.log(event.source.value);
    if (event.source.value == 'Child') {
      this.child = false;
    } else {
      this.child = true;
    }
  }

  selectChange(event) {
    console.log(event.source.value);

    if (event.source.value == 'day') {
      this.day = false;
      this.month = true;
      this.year = true;
    } else if (event.source.value == 'month') {
      this.month = false;
      this.day = true;
      this.year = true;
    } else if (event.source.value == 'year') {
      this.year = false;
      this.month = true;
      this.day = true;
    }
  }

  get f() {
    return this.usersreport.controls;
  }

  //search
  // onChangedPage(page: PageEvent) {
  //   console.log('PAGE EVENT', page);
  //   this.paginate.page = page.pageIndex;
  //   this.paginate.pageSize = page.pageSize;
  //   // console.log("PAGE LANG", page)
  //   this.fetchDataReport();
  // }
  // end

  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim();
  //   filterValue = filterValue.toLowerCase();
  //   this.paginate.searchString = filterValue;
  //   console.log('view search string', filterValue);

  //   this.paginator.pageIndex == 0
  //     ? this.fetchDataReport()
  //     : this.paginator.firstPage();
  // }

  onSubmit() {
    if (this.usersreport.invalid) {
      //this is to show all the required fields

      this.usersreport.markAllAsTouched();
      this.toastr.error('Please complete all required fields');

      return;
    } else {

      // let var_date = (this.usersreport.get('days').value == '') ? '' : moment(this.usersreport.get('days').value).format() ;

      // let days = var_date;
      // let years = this.usersreport.get('years').value;
      // let months = this.usersreport.get('months').value;
      let zones = this.usersreport.get('zones').value;
      // let person_status = this.usersreport.get('status').value;

      // console.log('days', days);

      this.getalldata
        .getAllPovertyReport(zones)
        .subscribe(async (data:any) => {
          console.log(data);

          var result: any = await data;

          this.getAllData = result;



            console.log('Get all report', result);


            console.log('Data', this.getAllData.data);
            // this.paginate.totalCount = result.data.totalCount;
            this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
            this.dataSource.data = this.getAllData.data;

        });
    }
  }

  //
  getAllPovertyAccount() {
    this.getalldata
      .getAllPovertyAccount(this.paginate)
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("getAllPovertyAccount", result)

        this.getAllData = result.data;

        console.log("Data", this.getAllData.data)
        this.paginate.totalCount = result.data.totalCount
        this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
        this.dataSource.data = this.getAllData.data;

        // this.dataSourceAll.data = this.getAllData;
      });
  }

  //chart

/*
//piechartUnderweight
pieChartFunction(){

  const DATA_COUNT = 5;
  const NUMBER_CFG = {count: DATA_COUNT, min: 0, max: 100};

  var ctx = document.getElementById('piechart');

  this.getalldata.getAllCountChildStatus().subscribe(async (data) => {


    const result: any = data.data[0];

    var myChart = new Chart('piechart', {


      type: 'doughnut',



      data: {
        labels: [
          '  Zone 1',
            'Zone 2',
             'Zone 3',
             'Zone 4',
              ' Zone 5',
               ' Zone 6',
               ' Zone 7'
        ],

        datasets: [
          {
            label: 'Dataset 1',






            data: [
              result.under_child_one,
              result.under_child_two,
              result.under_child_three,
              result.under_child_four,
              result.under_child_five,
              result.under_child_six,
              result.under_child_seven,





            ],
            backgroundColor: [
              '#40E0D0',
              '#F99A85',
              '#D2B4DE ',
              '#D1F2EB  ',
              '#26C6DA  ',
              '#FFE082 ',
              '#BCAAA4  ',
            ],


          },
        ],
      },
      options: {
        responsive: true,

        plugins: {
          legend: {
            position: 'top',


          },
          title: {
            display: true,

            text: 'Malnourish by Zone'
          }

        }
      },
    });




}); //end */


} //end function



