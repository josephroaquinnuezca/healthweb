

import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import {MatChipsModule} from '@angular/material/chips';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-send-announcement-fourps',
  templateUrl: './send-announcement-fourps.component.html',
  styleUrls: ['./send-announcement-fourps.component.scss']
})
export class SendAnnouncementFourpsComponent implements OnInit {


  commentform: FormGroup


  dialogData:any;

  DataValues:any;

  radioValue:any


  constructor(
    private formgroup: FormBuilder,
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<SendAnnouncementFourpsComponent>,
    private deleteSingleData: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }

  ngOnInit(): void {
    this.validation();
  }

    //validation of form
    validation() {
      this.commentform = this.formgroup.group({
        comment: [''],
        formattext: [''],
        // contacts: [this.dialogData.contact]

      });
    }


    radioChange($event: MatRadioChange) {
      console.log($event.source.name, $event.value);

      if ($event.value == 1) {
        this.radioValue = "Announcement from DSWD Parent Leader.We have a meeting in our Barangay Hall.Thankyou. "
      }


      if ($event.value == 2) {
        this.radioValue = "Good Day!.We have a clean up drive today.Please attend because attendance is a must."
      }

      if ($event.value == 3) {
        this.radioValue = "Good Day!.You may now withdraw your ATM for your payout."
      }

  }

  onSubmit(){


    const data = {
      comment: this.commentform.get('comment').value,
      formattext: this.commentform.get('formattext').value
    }

    console.log(data)

    this.deleteSingleData.createAnnouncementfourps(data).subscribe((data) => {
      console.log(data);

      this.dialogRef.close();


    });
  }

}
