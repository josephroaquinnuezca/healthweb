import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendAnnouncementFourpsComponent } from './send-announcement-fourps.component';

describe('SendAnnouncementFourpsComponent', () => {
  let component: SendAnnouncementFourpsComponent;
  let fixture: ComponentFixture<SendAnnouncementFourpsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendAnnouncementFourpsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendAnnouncementFourpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
