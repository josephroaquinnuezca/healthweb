import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendAnnouncementPregnantComponent } from './send-announcement-pregnant.component';

describe('SendAnnouncementPregnantComponent', () => {
  let component: SendAnnouncementPregnantComponent;
  let fixture: ComponentFixture<SendAnnouncementPregnantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendAnnouncementPregnantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendAnnouncementPregnantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
