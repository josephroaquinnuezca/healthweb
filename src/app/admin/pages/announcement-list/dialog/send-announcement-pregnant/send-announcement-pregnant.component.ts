
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import {MatChipsModule} from '@angular/material/chips';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-send-announcement-pregnant',
  templateUrl: './send-announcement-pregnant.component.html',
  styleUrls: ['./send-announcement-pregnant.component.scss']
})
export class SendAnnouncementPregnantComponent implements OnInit {


  commentform: FormGroup


  dialogData:any;

  DataValues:any;

  radioValue: any

  constructor(
    private formgroup: FormBuilder,
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<SendAnnouncementPregnantComponent>,
    private deleteSingleData: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }

  ngOnInit(): void {
    this.validation();
  }

    //validation of form
    validation() {
      this.commentform = this.formgroup.group({
        comment: [''],
        formattext: [''],
        // contacts: [this.dialogData.contact]

      });
    }
    radioChange($event: MatRadioChange) {
      console.log($event.source.name, $event.value);

      if ($event.value == 1) {
        this.radioValue ="Good Day!. Visit our RHU from Wednesday to Thursday for your Chcekup. Thankyou! "
      }

      if ($event.value == 2) {
        this.radioValue = " Announcement. All pregnant women must be inject of an anti tetanu. Visit our RHU ."
      }

      if ($event.value == 3) {
        this.radioValue = "Good Day!. We have a symposium for pregnant women. Please go to our public auditorium today."
      }

  }

  onSubmit(){


    const data = {
      comment: this.commentform.get('comment').value,
      formattext: this.commentform.get('formattext').value
    }


    console.log(data)


    this.deleteSingleData.createAnnouncementpregnant(data).subscribe((data) => {
      console.log(data);

      this.dialogRef.close();


    });
  }

}
