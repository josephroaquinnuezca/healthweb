import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendAnnouncementPwdComponent } from './send-announcement-pwd.component';

describe('SendAnnouncementPwdComponent', () => {
  let component: SendAnnouncementPwdComponent;
  let fixture: ComponentFixture<SendAnnouncementPwdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendAnnouncementPwdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendAnnouncementPwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
