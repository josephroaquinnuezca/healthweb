
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import {MatChipsModule} from '@angular/material/chips';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-send-announcement-pwd',
  templateUrl: './send-announcement-pwd.component.html',
  styleUrls: ['./send-announcement-pwd.component.scss']
})
export class SendAnnouncementPwdComponent implements OnInit {


  commentform: FormGroup


  dialogData:any;

  DataValues:any;

  radioValue: any

  constructor(
    private formgroup: FormBuilder,
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<SendAnnouncementPwdComponent>,
    private deleteSingleData: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }

  ngOnInit(): void {
    this.validation();
  }

    //validation of form
    validation() {
      this.commentform = this.formgroup.group({
        comment: [''],
        // contacts: [this.dialogData.contact]

      });
    }
    radioChange($event: MatRadioChange) {
      console.log($event.source.name, $event.value);

      if ($event.value == 1) {
        this.radioValue ="Announcement from RHU. Please go to public auditorium to get your fruits and other benefits. Thankyou  "
      }

      if ($event.value == 2) {
        this.radioValue = "Good Day!.You may now get the form in the RHU for your ID."
      }


    }



  onSubmit(){


    const data = {
      comment: this.commentform.get('comment').value
    }



    this.deleteSingleData.createAnnouncementpwd(data).subscribe((data) => {
      console.log(data);

      this.dialogRef.close();


    });
  }
}
