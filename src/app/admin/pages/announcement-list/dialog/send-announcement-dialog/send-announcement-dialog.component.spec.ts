import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendAnnouncementDialogComponent } from './send-announcement-dialog.component';

describe('SendAnnouncementDialogComponent', () => {
  let component: SendAnnouncementDialogComponent;
  let fixture: ComponentFixture<SendAnnouncementDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendAnnouncementDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendAnnouncementDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
