import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import {MatChipsModule} from '@angular/material/chips';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-send-announcement-dialog',
  templateUrl: './send-announcement-dialog.component.html',
  styleUrls: ['./send-announcement-dialog.component.scss']
})
export class SendAnnouncementDialogComponent implements OnInit {

  commentform: FormGroup


  dialogData:any;

  DataValues:any;

  radioValue:any

  constructor(
    private formgroup: FormBuilder,
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<SendAnnouncementDialogComponent>,
    private deleteSingleData: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }

  ngOnInit(): void {
    this.validation();
  }

    //validation of form
    validation() {
      this.commentform = this.formgroup.group({
        comment: [''],
        formattext: [''],
        // contacts: [this.dialogData.contact]

      });
    }


    radioChange($event: MatRadioChange) {
      console.log($event.source.name, $event.value);

      if ($event.value == 1) {
        this.radioValue = "Good Day!  We have a feeding program tomorrow morning.Will be held in our Barangay plaza. Thankyou. "
      }


      if ($event.value == 2) {
        this.radioValue = "Good Day!.  We will visit your house tomorrow for the deworming of your children. Thankyou."
      }

      if ($event.value == 3) {
        this.radioValue = "Good Day!.We will get the height and weight of your children it will be held in the barangay plasa.Thankyou."
      }
      if ($event.value == 4) {
        this.radioValue = "Good Day!.You may now get the vitamins of your children and some fruits in our barangay hall.Thankyou."
      }
  }

  onSubmit(){


    const data = {
      comment: this.commentform.get('comment').value,
      formattext: this.commentform.get('formattext').value
    }


    console.log(data)


    this.deleteSingleData.createAnnouncement(data).subscribe((data) => {
      console.log(data);

      this.dialogRef.close();


    });
  }

}
