import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendAnnouncementSeniorComponent } from './send-announcement-senior.component';

describe('SendAnnouncementSeniorComponent', () => {
  let component: SendAnnouncementSeniorComponent;
  let fixture: ComponentFixture<SendAnnouncementSeniorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendAnnouncementSeniorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendAnnouncementSeniorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
