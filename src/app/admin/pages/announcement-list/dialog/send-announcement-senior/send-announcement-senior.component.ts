import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import {MatChipsModule} from '@angular/material/chips';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'app-send-announcement-senior',
  templateUrl: './send-announcement-senior.component.html',
  styleUrls: ['./send-announcement-senior.component.scss']
})
export class SendAnnouncementSeniorComponent implements OnInit {


  commentform: FormGroup


  dialogData:any;

  DataValues:any;

  radioValue: any

  constructor(
    private formgroup: FormBuilder,
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<SendAnnouncementSeniorComponent>,
    private deleteSingleData: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }

  ngOnInit(): void {
    this.validation();
  }

    //validation of form
    validation() {
      this.commentform = this.formgroup.group({
        comment: [''],
        formattext: [''],
        // contacts: [this.dialogData.contact]

      });

    }
    radioChange($event: MatRadioChange) {
      console.log($event.source.name, $event.value);

      if ($event.value == 1) {
        this.radioValue ="Good Day!. Announcement from Brgy. secretary/BHW .Visit our Barangay Hall to get your maintenance. "
      }

      if ($event.value == 2) {
        this.radioValue = "Good Day!Announcement! According to your Senior Citizen President you have a meeting today in the barangay hall. Thankyou."
      }

      if ($event.value == 3) {
        this.radioValue = "Good Day!.You have a meeting today in the public auditorium. Thankyou."
      }
      if ($event.value == 4) {
        this.radioValue = "Good Day!. You may now get the vitamins of your children and some fruits in our barangay hall. Thankyou."
      }
    }

  onSubmit(){


    const data = {
      comment: this.commentform.get('comment').value,
      formattext: this.commentform.get('formattext').value
    }



    this.deleteSingleData.createAnnouncementsenior(data).subscribe((data) => {
      console.log(data);

      this.dialogRef.close();


    });
  }


}
