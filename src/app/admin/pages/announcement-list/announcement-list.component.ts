import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AdminserviceService } from '../../service/adminservice.service';
import { SendAnnouncementDialogComponent } from './dialog/send-announcement-dialog/send-announcement-dialog.component';
import { SendAnnouncementPregnantComponent } from './dialog/send-announcement-pregnant/send-announcement-pregnant.component';
import { SendAnnouncementSeniorComponent } from './dialog/send-announcement-senior/send-announcement-senior.component';

@Component({
  selector: 'app-announcement-list',
  templateUrl: './announcement-list.component.html',
  styleUrls: ['./announcement-list.component.scss']
})
export class AnnouncementListComponent implements OnInit {


  //
  Malnourished: any = [];
  Senior: any = [];
  Pregnant: any = [];
  Pwd:any = [];
  Fourps:any = [];


  constructor(
    private service: AdminserviceService,
    public dialog: MatDialog) {}

  ngOnInit(): void {

    this.getAllMalnourished();
    this.getAllSenior();
    this.getAllpregnant();
    this.getAllPwd();
    this.getallFourps();
  }


  getAllMalnourished() {
    this.service
      .getAllMalnourished()
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("malnuourished", result)

        this.Malnourished = result.data;


        // this.dataSourceAll.data = this.getAllData;
      });
  }

  getAllSenior() {
    this.service
      .getAllSenior()
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("Senior", result)

        this.Senior = result.data;


        // this.dataSourceAll.data = this.getAllData;
      });
  }

  getAllpregnant() {
    this.service
      .getAllpregnant()
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("Pregnant", result)

        this.Pregnant = result.data;


        // this.dataSourceAll.data = this.getAllData;
      });
  }

  getAllPwd() {
    this.service
      .getAllPwd()
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("Pwd", result)

        this.Pwd = result.data;


        // this.dataSourceAll.data = this.getAllData;
      });
  }

  getallFourps() {
    this.service
      .getallFourps()
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("Fourps", result)

        this.Fourps = result.data;


        // this.dataSourceAll.data = this.getAllData;
      });
  }



  sendMalnourished(){


    const dialogRef = this.dialog.open(SendAnnouncementDialogComponent,{

     });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


  sendSenior(){


    const dialogRef = this.dialog.open(SendAnnouncementSeniorComponent,{

     });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }



  sendPregnant(){


    const dialogRef = this.dialog.open(SendAnnouncementPregnantComponent,{

     });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }



  sendPwd(){


    const dialogRef = this.dialog.open(SendAnnouncementPregnantComponent,{

     });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  sendfourps(){


    const dialogRef = this.dialog.open(SendAnnouncementPregnantComponent,{

     });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }




}
