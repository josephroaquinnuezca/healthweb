import { MatDialog } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AddMedicineDialogComponent } from '../medicine-info/add-medicine-dialog/add-medicine-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle,
} from 'node_modules/chart.js';
import { AdminserviceService } from '../../service/adminservice.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort;

  //mat table
  dataSource: MatTableDataSource<any>;
  //mat table
  displayedColumns: string[] = [
    'no',
    'bacthno',
    'gname',
    'quantity',
    'manufacturer',
    'expiration',
    'status_date',
  ];

  //search
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };
  // dataSource = ELEMENT_DATA;

  // dataSource = ELEMENT_DATA;

  countdata: any;

  getAllData: any = [];
  today = moment(new Date()).format();

  constructor(
    private getalldata: AdminserviceService,
    public dialog: MatDialog
  ) {
    Chart.register(
      ArcElement,
      LineElement,
      BarElement,
      PointElement,
      BarController,
      BubbleController,
      DoughnutController,
      LineController,
      PieController,
      PolarAreaController,
      RadarController,
      ScatterController,
      CategoryScale,
      LinearScale,
      LogarithmicScale,
      RadialLinearScale,
      TimeScale,
      TimeSeriesScale,
      Decimation,
      Filler,
      Legend,
      Title,
      Tooltip,
      SubTitle
    );
  }

  ngOnInit(): void {
    this.getAllCountStatus();
    this.pieChartFunction();
    this.PregnantChartFunction();
    this.pieChartFunctionChild();
    this.PopulationChartFunctionChild();
    this.SeniorChartFunction();
    this.MaleChartFunction();
    this.FemaleChartFunction();
    this.getAllMedicineInfo();

    // calculation of remaining days


  }
  //child
  getAllCountStatus() {
    this.getalldata.getAllCountChildStatus().subscribe(async (data) => {
      console.log(data);

      var result: any = await data;

      this.countdata = result.data[0];

      console.log(this.countdata);

      // this.dataSourceAll.data = this.getAllData;
    });
  }
  //piechartUnderweight
  pieChartFunction() {
    const DATA_COUNT = 5;
    const NUMBER_CFG = { count: DATA_COUNT, min: 0, max: 100 };

    var ctx = document.getElementById('piechart');

    this.getalldata.getAllCountChildStatus().subscribe(async (data) => {
      const result: any = data.data[0];

      var myChart = new Chart('piechart', {
        type: 'doughnut',

        data: {
          labels: [
            '  Zone 1',
            'Zone 2',
            'Zone 3',
            'Zone 4',
            ' Zone 5',
            ' Zone 6',
            ' Zone 7',
          ],

          datasets: [
            {
              label: 'Dataset 1',

              data: [
                result.under_child_one,
                result.under_child_two,
                result.under_child_three,
                result.under_child_four,
                result.under_child_five,
                result.under_child_six,
                result.under_child_seven,
              ],
              backgroundColor: [
                '#40E0D0',
                '#F99A85',
                '#D2B4DE ',
                '#D1F2EB  ',
                '#26C6DA  ',
                '#FFE082 ',
                '#BCAAA4  ',
              ],
            },
          ],
        },
        options: {
          responsive: true,

          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,

              text: 'Malnourish by Zone',
            },
          },
        },
      });
    }); //end
  } //end function

  //piechartChildren
  pieChartFunctionChild() {
    const DATA_COUNT = 5;
    const NUMBER_CFG = { count: DATA_COUNT, min: 0, max: 100 };

    var ctx = document.getElementById('radarchartChild');

    this.getalldata.getAllCountChildStatus().subscribe(async (data) => {
      const result: any = data.data[0];

      var myChart = new Chart('radarchartChild', {
        type: 'polarArea',
        data: {
          labels: [
            '  Zone 1',
            'Zone 2',
            'Zone 3',
            'Zone 4',
            ' Zone 5',
            ' Zone 6',
            ' Zone 7',
          ],
          datasets: [
            {
              label: 'Children',

              data: [
                result.child_one,
                result.child_two,
                result.child_three,
                result.child_four,
                result.child_five,
                result.child_six,
                result.child_seven,
              ],
              backgroundColor: [
                '#FFEBEE',
                '#F99A85',
                '#F3E5F5  ',
                '#E3F2FD   ',
                '#E0F2F1 ',
                '#E8F5E9 ',
                '#FFF9C4   ',
              ],
            },
          ],
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Children by Zone',
            },
          },
        },
      });
    }); //end
  } //end function

  //WholePopulation

  //Population

  PopulationChartFunctionChild() {
    const DATA_COUNT = 5;
    const NUMBER_CFG = { count: DATA_COUNT, min: 0, max: 100 };

    var ctx = document.getElementById('radarchartPopulation');

    this.getalldata.getAllCountPopulationStatus().subscribe(async (data) => {
      const result: any = data.data[0];

      var myChart = new Chart('radarchartPopulation', {
        type: 'radar',
        data: {
          labels: [
            '  Zone 1',
            'Zone 2',
            'Zone 3',
            'Zone 4',
            ' Zone 5',
            ' Zone 6',
            ' Zone 7',
            'Total population',
          ],
          datasets: [
            {
              label: 'Population',

              data: [
                result.zone_one,
                result.zone_two,
                result.zone_three,
                result.zone_four,
                result.zone_five,
                result.zone_six,
                result.zone_seven,
                result.population,
              ],
              backgroundColor: [
                '#FFEBEE',
                '#F99A85',
                '#F3E5F5  ',
                '#E3F2FD   ',
                '#E0F2F1 ',
                '#E8F5E9 ',
                '#FFF9C4   ',
              ],
            },
          ],
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,
              text: 'Total Population',
            },
          },
        },
      });
    }); //end
  } //end function

  MaleChartFunction() {
    const DATA_COUNT = 5;
    const NUMBER_CFG = { count: DATA_COUNT, min: 0, max: 100 };

    var ctx = document.getElementById('malechart');

    this.getalldata.getAllCountMaleZone().subscribe(async (data) => {
      const result: any = data.data[0];

      var myChart = new Chart('malechart', {
        type: 'bar',

        data: {
          labels: [
            '  Zone 1',
            'Zone 2',
            'Zone 3',
            'Zone 4',
            ' Zone 5',
            ' Zone 6',
            ' Zone 7',
            'Total',
          ],

          datasets: [
            {
              label: 'Male',

              data: [
                result.zone_one_male,
                result.zone_two_male,
                result.zone_three_male,
                result.zone_four_male,
                result.zone_five_male,
                result.zone_six_male,
                result.zone_seven_male,
                result.Male,
              ],
              backgroundColor: [
                '#40E0D0',
                '#F99A85',
                '#D2B4DE ',
                '#D1F2EB  ',
                '#26C6DA  ',
                '#FFE082 ',
                '#BCAAA4  ',
              ],
            },
          ],
        },
        options: {
          responsive: true,

          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,

              text: 'Population of Male per Zone',
            },
          },
        },
      });
    }); //end
  } //end function

  FemaleChartFunction() {
    const DATA_COUNT = 5;
    const NUMBER_CFG = { count: DATA_COUNT, min: 0, max: 100 };

    var ctx = document.getElementById('femalechart');

    this.getalldata.getAllCountFemaleZone().subscribe(async (data) => {
      const result: any = data.data[0];

      var myChart = new Chart('femalechart', {
        type: 'bar',

        data: {
          labels: [
            '  Zone 1',
            'Zone 2',
            'Zone 3',
            'Zone 4',
            ' Zone 5',
            ' Zone 6',
            ' Zone 7',
            'Total',
          ],

          datasets: [
            {
              label: 'Female',

              data: [
                result.zone_one_female,
                result.zone_two_female,
                result.zone_three_female,
                result.zone_four_female,
                result.zone_five_female,
                result.zone_six_female,
                result.zone_seven_female,
                result.Total,
              ],
              backgroundColor: [
                '#40E0D0',
                '#F99A85',
                '#D2B4DE ',
                '#D1F2EB  ',
                '#26C6DA  ',
                '#FFE082 ',
                '#BCAAA4  ',
              ],
            },
          ],
        },
        options: {
          responsive: true,

          plugins: {
            legend: {
              position: 'top',
            },
            title: {
              display: true,

              text: 'Population of Female per Zone',
            },
          },
        },
      });
    }); //end
  } //end function

  PregnantChartFunction() {
    var ctx = document.getElementById('myChart');

    this.getalldata.getAllCountPregnant().subscribe((data) => {
      const result: any = data.data[0];

      console.log('data one ', result);

      var myChart = new Chart('myChart', {
        type: 'bar',
        data: {
          labels: [
            'Zone 1',
            'Zone 2',
            'Zone 3',
            'Zone 4',
            'Zone 5',
            'Zone 6',
            'Zone 7',
            'Total ',
          ],
          datasets: [
            {
              label: 'Pregnant',
              data: [
                result.count_zone_one,
                result.count_zone_two,
                result.count_zone_three,
                result.count_zone_four,
                result.count_zone_five,
                result.count_zone_six,
                result.count_zone_seven,
                result.pregnant,
              ],
              backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 99, 132, 0.2)',
              ],
              borderColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
              ],
              borderWidth: 1,
            },
          ],
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
            },
          },
        },
      });
    }); //end
  }

  //Senior
  SeniorChartFunction() {
    var ctx = document.getElementById('myChart');

    this.getalldata.getAllCountSenior().subscribe((data) => {
      const result: any = data.data[0];

      console.log('data one ', result);

      var myChart = new Chart('seniorChart', {
        type: 'line',
        data: {
          labels: [
            'Zone 1',
            'Zone 2',
            'Zone 3',
            'Zone 4',
            'Zone 5',
            'Zone 6',
            'Zone 7',
            'Total',
          ],
          datasets: [
            {
              label: 'Senior',
              data: [
                result.count_senior_one,
                result.count_senior_two,
                result.count_senior_three,
                result.count_senior_four,
                result.count_senior_five,
                result.count_senior_six,
                result.count_senior_seven,
                result.senior,
              ],

              borderWidth: 1,
              borderColor: 'rgb(75, 192, 192)',
            },
          ],
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
            },
          },
        },
      });
    }); //end

    //medicine
  }

  getAllMedicineInfo() {
    this.getalldata
      .getRemainingStocks()
      .subscribe(async (data) => {
        console.log(data);

        var result: any = await data;

        console.log('Medicine', result);

        this.getAllData = result.data;





        // const startdate = this.getAllData.data.manufacturingdate
        // const enddate = this.getAllData.data.exdate


        // console.log(enddate)
        // console.log(startdate)

        // const diffTime = startdate.getTime() - enddate.getTime();
        // const resultda = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        // console.log("result data", resultda)

        // console.log('Data', this.getAllData.data);
        // this.paginate.totalCount = result.data.totalCount;
        // this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
        // this.dataSource.data = this.getAllData.data;


        // for (var char of result) {
        //   console.log(char); // prints chars: H e l l o  W o r l d
        // }

        // this.dataSourceAll.data = this.getAllData;
      });
  }
}
