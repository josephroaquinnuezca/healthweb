import { Component, OnInit ,Inject} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

@Component({
  selector: 'app-patient-view',
  templateUrl: './patient-view.component.html',
  styleUrls: ['./patient-view.component.scss']
})
export class PatientViewComponent implements OnInit {



  DataValues:any=[];

  dialogData:any;


  constructor(
     // private clientService: ClientServiceService,
     private getAlldata: AdminserviceService,
     @Inject(MAT_DIALOG_DATA) public results,

  ) {
    this.dialogData = results
   }

  ngOnInit(): void {




    this.getAlldata.getSingleMedication(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data

      console.log("results", result)

      // this.getAllClient();
    });
    console.log(this.dialogData.id)
  }

}
