import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientAddDialogComponent } from './patient-add-dialog.component';

describe('PatientAddDialogComponent', () => {
  let component: PatientAddDialogComponent;
  let fixture: ComponentFixture<PatientAddDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientAddDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
