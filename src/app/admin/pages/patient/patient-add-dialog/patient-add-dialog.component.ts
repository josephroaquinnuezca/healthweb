import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup , Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';

@Component({
  selector: 'app-patient-add-dialog',
  templateUrl: './patient-add-dialog.component.html',
  styleUrls: ['./patient-add-dialog.component.scss']
})
export class PatientAddDialogComponent implements OnInit {
  //add
  medicineform: FormGroup
   //search
   pageOptionSize: any = [5, 10, 20];
   paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: ''
  }

  //
  getAllData: any = [];


  singleData: any;

  getSelectedData: any

  dialogData:any;


   onchangeValue: any

   quantityshow = true;

    today = moment(new Date()).format();


  constructor(
    private getalldata: AdminserviceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<PatientAddDialogComponent>,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public results,
    ) {
      this.dialogData = results
    }

  ngOnInit(): void {
    this.getAllMedicineInfo();
    this.AddUserFormValidation();

    this.getalldata.ViewSingleUserInfo(this.dialogData.id).subscribe(data =>{
      console.log("Get Single data", data);

      const dataform: any = data

      this.singleData = dataform.data

    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });


  }

   //validation of form
   AddUserFormValidation() {
    this.medicineform = this.formgroup.group({
      med_id: [''],
      med_dosage: [''],
      med_name: [''],
      med_type: [''],
      med_available: [''],
      med_manufacturing: [''],
      med_exdate: [''],
      quantity: ['', [ Validators.pattern('^[0-2]*$'),
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(1)]],
      unit: [''],


    });
  }


  // onAdd(){

  // }

  onSubmit(){
    if (this.medicineform.invalid) {
      //this is to show all the required fields

      // this.medicineform.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      return;
    } else {


     const data: any = {

      med_id: this.medicineform.get('med_id').value,
      patient_id: this.dialogData.id,
      quantity: this.medicineform.get('quantity').value,
      unit: this.medicineform.get('unit').value,
      med_available: this.medicineform.get('med_available').value,
      med_type: this.medicineform.get('med_type').value,
      med_dosage: this.medicineform.get('med_dosage').value,
      med_manufacturing: this.medicineform.get('med_manufacturing').value,
      med_exdate: this.medicineform.get('med_exdate').value,





     }

     const med_expiration  = this.medicineform.get('med_exdate').value

     const date_today = new Date();

//validation for expiration date..
     const date_format = moment(date_today).format();

     if(med_expiration <= date_format){
      this.toastr.error('The medicine is expired');
     }else{
      this.getalldata.createInventoryMedic(data).subscribe(data =>{
        console.log(data);
        this.toastr.success('Successfully Inserted');
        this.dialogRef.close();
      }, err => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status)
          console.log("hello",err.error.description)
          this.toastr.error(err.error.description);
        }else{
          console.log(err)
        }

      }, () => {
        console.log('request completed')
      });
     }






    }
  }

  getAllMedicineInfo() {
    this.getalldata
      .getAllMeds()
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("Medicine", result)

        this.getAllData = result.data;

        console.log("Data Medicine", this.getAllData.data)
      });
  }

    selectMed(event){
      console.log("event value", event.value)


      this.getalldata.getSingleMedicine(event.value).subscribe(data =>{
        console.log("Get Single data", data);


        this.getSelectedData = data;

        this.quantityshow = false




      }, err => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status)
          console.log("hello",err.error.description)

        }else{
          console.log(err)
        }

      }, () => {
        console.log('request completed')
      });
    }

  quantityChange(event, quantity){

  console.log(quantity)


  //  const available_stock = this.medicineform.get('med_available').value

  //  console.log(available_stock)

  //

   if( event.target.value >= quantity){
    this.toastr.error('The quantity is greater than the available stocks');
    //  this._snackBar.open("The quantity is greater than the available stocks");
   }else{


    this.toastr.success('Good');
    const result = quantity - event.target.value;

    this.medicineform.get("med_available").patchValue(result)

   }


  }





}
