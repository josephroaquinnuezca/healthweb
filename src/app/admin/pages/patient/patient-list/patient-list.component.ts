import { PatientViewComponent } from './../patient-view/patient-view.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import { DeleteUserDialogComponent } from '../../user-info/delete-user-dialog/delete-user-dialog.component';
import { EditUserDialogComponent } from '../../user-info/edit-user-dialog/edit-user-dialog.component';
import { ViewUserDialogComponent } from '../../user-info/view-user-dialog/view-user-dialog.component';
import { PatientAddDialogComponent } from '../patient-add-dialog/patient-add-dialog.component';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {






  @ViewChild('paginator', { static: false}) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort


  //mat table
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['no', 'fullname', 'age', 'sex', 'address', 'type', 'date', 'controls'];
  // dataSource = ELEMENT_DATA;


   //search
   pageOptionSize: any = [5, 10, 20];
   paginate: any = {
   totalCount!: 1100,
    pageSize: 10,
    page: 0,
    searchString: ''
  }

  //
  getAllData: any = [];



  constructor(
    public dialog: MatDialog,
    private getalldata: AdminserviceService
    ) {}

  ngOnInit(): void {
    this.getAllClientAppointmentAdmin();
  }

  //search
  onChangedPage(page: PageEvent) {
    console.log("PAGE EVENT", page)
    this.paginate.page = page.pageIndex
    this.paginate.pageSize = page.pageSize
    // console.log("PAGE LANG", page)
    this.getAllClientAppointmentAdmin();

  }
  // end

  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log("view search string", filterValue)

    this.paginator.pageIndex == 0
      ? this.getAllClientAppointmentAdmin()
      : this.paginator.firstPage();
  }

  getAllClientAppointmentAdmin() {
    this.getalldata
      .getAllUserInfo(this.paginate)
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("getAllClientAppointmentAdmin", result)

        this.getAllData = result.data;

        console.log("Data", this.getAllData.data)
        this.paginate.totalCount = result.data.totalCount
        this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
        this.dataSource.data = this.getAllData.data;

        // this.dataSourceAll.data = this.getAllData;
      });
  }

  adduser(id){
    const dialogRef = this.dialog.open(PatientAddDialogComponent,{
        data: {
          id,
        },
     });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  deleteUser(id){
    const dialogRef = this.dialog.open(DeleteUserDialogComponent,{
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllClientAppointmentAdmin()
      console.log(`Dialog result: ${result}`);
    });
  }


  //edit User
  editUser(id){
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllClientAppointmentAdmin();
      console.log(`Dialog result: ${result}`);
    });
  }

  //view user
  viewUser(id){
    // const dialogRef = this.dialog.open(ViewUserDialogComponent);

    const dialogRef = this.dialog.open(PatientViewComponent, {
      data: {
        id,
      },
    });


    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }


}
