import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import { AdminInterface } from 'src/app/shared/interface/admin-interface';


interface Gender {
  value: string;
  viewValue: string;
}

interface Zone {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent implements OnInit {


  //add
  formAddUser: FormGroup

  //age
  showfemale = false;
  senior = false;
  child = false;
  pregnant: any;
  teenage:any;

  hidecalculate = true
  showBtn = true;

  TotalValueH: any;
  TotalValueW: any;

  type: any;


  showPregnantLastPeriod = false;
  showSeniorMaintenance = false;

  dialogData:any;


  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();

  foods: Gender[] = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'},
  ];

  zone: Zone[] = [
    {value: '1', viewValue: 'Zone 1'},
    {value: '2', viewValue: 'Zone 2'},
    {value: '3', viewValue: 'Zone 3'},
    {value: '4', viewValue: 'Zone 4'},
    {value: '5', viewValue: 'Zone 5'},
    {value: '6', viewValue: 'Zone 6'},
    {value: '7', viewValue: 'Zone 7'},
  ];

  constructor(
    public dialogRef: MatDialogRef<EditUserDialogComponent>,
    public UserInfoService: AdminserviceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    private spinner: NgxSpinnerService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {

    this.dialogData = results
  }

  ngOnInit(): void {
    this.AddUserFormValidation() //declare the functions that is created below
    // this.bmiFunction()

    this.ViewSingleData();


  }

  //
  get f() {
    return this.formAddUser.controls;
  }

  //validation of form
  AddUserFormValidation() {
    this.formAddUser = this.formgroup.group({
      fname: ['', Validators.required],
      mname: ['', Validators.required],
      lname: ['', Validators.required],
      suffix: [''],
      bdate: ['', Validators.required],
      age: [''],
      sex: ['', Validators.required],
      house_no: [''],
      zone: [''],
      province: [''],
      city: [''],
      barangay: [''],
      cell_no: ['',
      [ Validators.required, Validators.pattern('^[0-9]*$'),
      Validators.minLength(11),
      Validators.maxLength(11)]],
      childheight: [''],
      childkilo: [''],
      childmassindex: [''],
      maintenance_senior: [''],
      maintenance_pregnant: [''],
      pregnant_status: [''],
      type_maintenance  : [''],
      last_period: [''],

    });
  }

  //pregnant change event
  changePregnantRadio(event: MatRadioChange) {

    console.log(event.source.value);

    const pregantChange = event.source.value

    if(pregantChange == "Yes"){
      this.showPregnantLastPeriod = true;
    }

  }

  //senior
  cahngeSeniorRadio(event: MatRadioChange) {

    console.log(event.source.value);

    const seniorChange = event.source.value

    if(seniorChange == 1){
      this.showSeniorMaintenance = true;
    }

  }


  genderOnChange(event: any){

    if(event.value == "Female"){

      if(this.formAddUser.get('age').value < 13){
        this.showfemale = false

      }
      else if (this.formAddUser.get('age').value <=51){
        this.showfemale = true
        this.senior = false
      }
      else if (this.formAddUser.get('age').value >=52){
        this.showfemale = false
        this.senior = true
      }

      else {

        this.showfemale = true

      }

    } else {
      this.showfemale = false
    }
    console.log("gender", event.value)
  }

  //submit button execution

  onSubmit() {
    if (this.formAddUser.invalid) {
      //this is to show all the required fields
      console.log("Pregnant", this.formAddUser.get('maintenance_pregnant').value)
      this.formAddUser.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      // this.toastr.error('Please complete all required fields');
      return;
    } else {

      console.log("Pregnant", this.formAddUser.get('maintenance_pregnant').value)

      const data = {

        fname: this.formAddUser.get('fname').value,
        mname: this.formAddUser.get('mname').value,
        lname: this.formAddUser.get('lname').value,
        suffix: this.formAddUser.get('suffix').value,
        bdate: this.formAddUser.get('bdate').value,
        age: this.formAddUser.get('age').value,
        sex: this.formAddUser.get('sex').value,
        house_no: this.formAddUser.get('house_no').value,
        zone: this.formAddUser.get('zone').value,
        city: this.formAddUser.get('city').value,
        province: this.formAddUser.get('province').value,
        barangay: this.formAddUser.get('barangay').value,
        cell_no: this.formAddUser.get('cell_no').value
        // child_height: this.formAddUser.get('childheight').value,
        // child_weight: this.formAddUser.get('childkilo').value,
        // child_body_mass_index: this.formAddUser.get('childmassindex').value,
        // child_status: this.formAddUser.get('childstatus').value,
        // type: this.type,
        // maintenance_senior: this.formAddUser.get('maintenance_senior').value,
        // maintenance_pregnant: this.formAddUser.get('maintenance_pregnant').value,
        // pregnant_status: this.formAddUser.get('pregnant_status').value ,
        // last_period: this.formAddUser.get('last_period').value,
        // type_maintenance: this.formAddUser.get('type_maintenance').value,

      }

      const stringify = JSON.stringify(data);
      console.log("With Stringify :" , stringify);


      console.log(data)


    this.UserInfoService.updateSingleUserInfo(data, this.dialogData.id).subscribe(data =>{
          console.log(data);
          this.toastr.success('Successfully Inserted');
          this.spinner.show();

          setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.dialogRef.close();
            this.spinner.hide();
          }, 5000);
          // this.router.navigate(['/login']);
        }, err => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status)
            console.log("hello",err.error.description)
            this.toastr.error(err.error.description);
          }else{
            console.log(err)
          }

        }, () => {
          console.log('request completed')
        });



      // const username = this.formlogin.get('username').value;
      // const password = this.formlogin.get('password').value;

      // console.log('LOGIN VALUE', username, password);


    }
  }//end

  onDateChange(event:  MatDatepickerInputEvent<Date>) {

    const bdate = moment(event.value).format();

    var timeDiff = Math.abs(Date.now() - new Date(bdate).getTime());
    const age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);

    this.formAddUser.get("age").patchValue(age);


    this.formAddUser.controls['sex'].reset()

    if(age <= 12) {
      this.showBtn = false;
      this.type = 0;
      this.child = true
      this.showfemale = false
      this.hidecalculate = true;

    } else  if(age <=51 ){
      this.child = false
      this.senior = false
      this.showfemale = false
      this.hidecalculate = false;
      this.showBtn = true
    }

    else  if(age >= 52){
      this.type = 1;
      this.senior = true
      this.child = false
      this.showfemale = false
      this.showBtn = true
    }

    else {
      this.type = 2;
      this.senior = false
      this.child = false
    }

    console.log('Teste', age);
  }


  calculate(){


    // console.log("Bmi", this.formAddUser.get('child_height').value )



    if(this.formAddUser.get('childheight').value === ""){

      this.showBtn = false;
      setTimeout (() => {
        //          this.dialogRef.close();

        this.toastr.error('Height in (m) is reqired ');
              }, 1000);

    } else if (this.formAddUser.get('childkilo').value === "" ){
      this.showBtn = false;
      setTimeout (() => {
        //          this.dialogRef.close();
        this.toastr.error('Weight in (m) is reqired ');
              }, 1000);
    }else {



      const h = this.formAddUser.get('childheight').value
      const w =  this.formAddUser.get('childkilo').value

      const totalH = h * h;

      const divideTotal =  w / totalH


      this.formAddUser.get("childmassindex").setValue(divideTotal);

      this.hidecalculate = false;
      this.showBtn = true;


    }




  }

  onchangeH(event) {
    console.log("change", event.target.value)
    this.TotalValueH = event.target.value;
  }

  onchangeW(event) {
    this.TotalValueW  = event.target.value;
    console.log("change", event.target.value)
  }

  // bmiFunction(){
  //   this.TotalValueH
  //   this.TotalValueH
  //   console.log("bmiFunction", this.TotalValueH)
  // }

  ViewSingleData(){

    const id =  this.dialogData.id



    this.UserInfoService.ViewSingleUserInfo(id).subscribe(data =>{
      console.log("Get Single data", data);


      const dataform: any = data


      this.formAddUser.get("fname").patchValue(dataform?.data.fname);
      this.formAddUser.get("mname").patchValue(dataform?.data.mname);
      this.formAddUser.get("lname").patchValue(dataform?.data.lname);
      this.formAddUser.get("suffix").patchValue(dataform?.data.suffix);
      this.formAddUser.get("bdate").patchValue(dataform?.data.bdate);
      this.formAddUser.get("age").patchValue(dataform?.data.age);
      this.formAddUser.get("sex").patchValue(dataform?.data.sex);
      this.formAddUser.get("house_no").patchValue(dataform?.data.house_no);
      this.formAddUser.get("zone").patchValue(dataform?.data.zone);
      this.formAddUser.get("city").patchValue(dataform?.data.city);
      this.formAddUser.get("province").patchValue(dataform?.data.province);
      this.formAddUser.get("barangay").patchValue(dataform?.data.barangay);
      this.formAddUser.get("cell_no").patchValue(dataform?.data.cell_no);

      // this.formAddUser.get("child_height").patchValue(dataform?.data.child_height);
      // this.formAddUser.get("child_weight").patchValue(dataform?.data.child_weight);
      // this.formAddUser.get("child_body_mass_index").patchValue(dataform?.data.child_body_mass_index);
      // this.formAddUser.get("type").patchValue(dataform?.data.type);
      // this.formAddUser.get("maintenance_senior").patchValue(dataform?.data.maintenance_senior);
      // this.formAddUser.get("maintenance_pregnant").patchValue(dataform?.data.maintenance_pregnant);
      // this.formAddUser.get("pregnant_status").patchValue(dataform?.data.pregnant_status);
      // this.formAddUser.get("last_period").patchValue(dataform?.data.last_period);
      // this.formAddUser.get("type_maintenance").patchValue(dataform?.data.type_maintenance);


      // fname: this.formAddUser.get('fname').value,
      // mname: this.formAddUser.get('mname').value,
      // lname: this.formAddUser.get('lname').value,
      // suffix: this.formAddUser.get('suffix').value,
      // bdate: this.formAddUser.get('bdate').value,
      // age: this.formAddUser.get('age').value,
      // sex: this.formAddUser.get('sex').value,
      // house_no: this.formAddUser.get('house_no').value,
      // zone: this.formAddUser.get('zone').value,
      // city: this.formAddUser.get('city').value,
      // province: this.formAddUser.get('province').value,
      // barangay: this.formAddUser.get('barangay').value,
      // cell_no: this.formAddUser.get('cell_no').value,
      // child_height: this.formAddUser.get('childheight').value,
      // child_weight: this.formAddUser.get('childkilo').value,
      // child_body_mass_index: this.formAddUser.get('childmassindex').value,
      // type: this.type,
      // maintenance_senior: this.formAddUser.get('maintenance_senior').value,
      // maintenance_pregnant: this.formAddUser.get('maintenance_pregnant').value,
      // pregnant_status: this.formAddUser.get('pregnant_status').value ,
      // last_period: this.formAddUser.get('last_period').value,
      // type_maintenance: this.formAddUser.get('type_maintenance').value,






    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
        this.toastr.error(err.error.description);
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });


  }





}
