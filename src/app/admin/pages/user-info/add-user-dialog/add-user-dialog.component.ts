import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  NgModule,
} from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

// import {moment} from 'moment/moment';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AdminInterface } from 'src/app/shared/interface/admin-interface';
import { MatDialogRef } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';

import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormsModule } from '@angular/forms';

//gender
interface Gender {
  value: string;
  viewValue: string;
}
//for zone
interface Zone {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss'],
})
export class AddUserDialogComponent implements OnInit {
  /*
  @NgModule({
    imports: [
        FormsModule
    ]})
  months = [
    { value: 1, viewValue: 'January' },
    { value: 2, viewValue: 'February' },
    { value: 3, viewValue: 'March' },
    { value: 4, viewValue: 'April' },
    { value: 5, viewValue: 'May' },
    { value: 6, viewValue: 'June' },
    { value: 7, viewValue: 'July' },
    { value: 8, viewValue: 'August' },
    { value: 9, viewValue: 'September' },
    { value: 10, viewValue: 'October' },
    { value: 11, viewValue: 'November' },
    { value: 12, viewValue: 'December' },

  ];
  selectedMonth: number;

*/

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '200px',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    // upload: (file: File) => { ... }?
    // uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']],
  };
  //add
  formAddUser: FormGroup;

  //age
  showfemale = false;
  senior = false;
  child = false;
  pregnant: any;
  teenage: any;

  hidecalculate = true;
  showBtn = true;
  showreset = false;

  TotalValueH: any;
  TotalValueW: any;

  type: any;

  getAllData: any = [];

  showPregnantLastPeriod = false;
  showSeniorMaintenance = false;

  person_status: any;

  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();

  foods: Gender[] = [
    { value: 'Male', viewValue: 'Male' },
    { value: 'Female', viewValue: 'Female' },
  ];

  zone: Zone[] = [
    { value: '1', viewValue: 'Zone 1' },
    { value: '2', viewValue: 'Zone 2' },
    { value: '3', viewValue: 'Zone 3' },
    { value: '4', viewValue: 'Zone 4' },
    { value: '5', viewValue: 'Zone 5' },
    { value: '6', viewValue: 'Zone 6' },
    { value: '7', viewValue: 'Zone 7' },
  ];

//   send_date=new Date();
//  formattedDate : any;

  constructor(
    public dialogRef: MatDialogRef<AddUserDialogComponent>,
    public UserInfoService: AdminserviceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.AddUserFormValidation(); //declare the functions that is created below
    // this.bmiFunction()


    // this.formattedDate = this.send_date.toISOString().slice(0, 10);
    // console.log(this.formattedDate);
  }

  //
  get f() {
    return this.formAddUser.controls;
  }

  //validation of form
  AddUserFormValidation() {
    this.formAddUser = this.formgroup.group({
      fname: ['', Validators.required],
      mname: [''],
      lname: ['', Validators.required],
      suffix: [''],
      bdate: ['', Validators.required],
      age: [''],
      sex: ['', Validators.required],
      house_no: ['', Validators.required],
      zone: ['', Validators.required],
      province: [''],
      city: [''],
      barangay: [''],
      cell_no: [
        '',
        [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          Validators.minLength(11),
          Validators.maxLength(11),
        ],
      ],
      pwd: [''],
      systolic: [''],
      diastolic: [''],
      medical_history: [''],
      childheight: [''],
      childkilo: [''],
      childmassindex: [''],
      childstatus: [''],
      maintenance_senior: [''],
      maintenance_pregnant: [''],
      pregnant_status: [''],
      type_maintenance: [''],
      last_period: [''],
      estimated_period: [''],
    });
  }

  //pregnant change event
  changePregnantRadio(event: MatRadioChange) {
    console.log(event.source.value);

    const pregantChange = event.source.value;

    if (pregantChange == 'Yes') {
      this.person_status = 'pregnant';
      this.showPregnantLastPeriod = true;
    } else {
      this.showPregnantLastPeriod = false;
    }
  }

  //senior
  cahngeSeniorRadio(event: MatRadioChange) {
    console.log(event.source.value);

    const seniorChange = event.source.value;

    if (seniorChange == 1) {
      this.showSeniorMaintenance = true;
    } else {
      this.showSeniorMaintenance = false;
    }
  }

  genderOnChange(event: any) {
    if (event.value == 'Female') {
      if (this.formAddUser.get('age').value < 13) {
        this.showfemale = false;
      } else if (this.formAddUser.get('age').value <= 51) {
        this.showfemale = true;
        this.senior = false;
      } else if (this.formAddUser.get('age').value >= 52) {
        this.showfemale = false;
        this.senior = true;
      } else {
        this.showfemale = true;
      }
    } else {
      this.showfemale = false;
    }
    console.log('gender', event.value);
  }

  //submit button execution

  onSubmit() {
    if (this.formAddUser.invalid) {
      //this is to show all the required fields
      // console.log(
      //   'Pregnant',
      //   this.formAddUser.get('maintenance_pregnant').value
      // );
      this.formAddUser.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      // this.toastr.error('Please complete all required fields');

      return;
    } else {
      // console.log(
      //   'Pregnant',
      //   this.formAddUser.get('maintenance_pregnant').value
      // );

      const data = {
        fname: this.formAddUser.get('fname').value,
        mname: this.formAddUser.get('mname').value,
        lname: this.formAddUser.get('lname').value,
        suffix: this.formAddUser.get('suffix').value,
        bdate: this.formAddUser.get('bdate').value,
        age: this.formAddUser.get('age').value,
        sex: this.formAddUser.get('sex').value,
        house_no: this.formAddUser.get('house_no').value,
        zone: this.formAddUser.get('zone').value,
        city: this.formAddUser.get('city').value,
        province: this.formAddUser.get('province').value,
        barangay: this.formAddUser.get('barangay').value,
        cell_no: this.formAddUser.get('cell_no').value,
        pwd: this.formAddUser.get('pwd').value,
        systolic: this.formAddUser.get('systolic').value,
        diastolic: this.formAddUser.get('diastolic').value,
        medical_history: this.formAddUser.get('medical_history').value,
        child_height: this.formAddUser.get('childheight').value,
        child_weight: this.formAddUser.get('childkilo').value,
        child_body_mass_index: this.formAddUser.get('childmassindex').value,
        child_status: this.formAddUser.get('childstatus').value,
        type: this.type,
        maintenance_senior: this.formAddUser.get('maintenance_senior').value,
        maintenance_pregnant: this.formAddUser.get('maintenance_pregnant')
          .value,
        pregnant_status: this.formAddUser.get('pregnant_status').value,
        last_period: this.formAddUser.get('last_period').value,
        estimated_period: this.formAddUser.get('estimated_period').value,
        type_maintenance: this.formAddUser.get('type_maintenance').value,
        person_status: this.person_status,
      };

      const stringify = JSON.stringify(data);

      //start validation for last period.
      const last_period = this.formAddUser.get('last_period').value;
      const date_today = new Date();
      //validation for last period.
      const date_format = moment(date_today).format();

      // if (last_period >= date_format) {
      //   this.toastr.error('Last period is Invalid');
      // } else {
      //   this.UserInfoService.createUserInfo(data).subscribe(
      //     (data) => {
      //       console.log(data);
      //       this.toastr.success('Successfully Inserted');
      //       this.dialogRef.close();
      //     },
      //     (err) => {
      //       if (err instanceof HttpErrorResponse) {
      //         console.log(err.status);
      //         console.log('hello', err.error.description);
      //         this.toastr.error(err.error.description);
      //       } else {
      //         console.log(err);
      //       }
      //     },
      //     () => {
      //       console.log('request completed');
      //     }
      //   );
      // }
      //end kaso dika gumagana

      console.log('With Stringify :', stringify);

      console.log(data);

      if (data.age <= 12) {
        if (data.child_height == '' &&  data.child_weight == ''  ) {
          this.toastr.error('Please complete all required fields');
        } else {
          this.UserInfoService.createUserInfo(data).subscribe(
            (data) => {
              console.log(data);
              this.toastr.success('Successfully Inserted');
              this.spinner.show();

              setTimeout(() => {
                /** spinner ends after 5 seconds */
                this.dialogRef.close();
                this.spinner.hide();
              }, 5000);
              // this.router.navigate(['/login']);
            },
            (err) => {
              if (err instanceof HttpErrorResponse) {
                console.log(err.status);
                console.log('hello', err.error.description);
                this.toastr.error(err.error.description);
              } else {
                console.log(err);
              }
            },
            () => {
              console.log('request completed');
            }
          );
        }
      } else {
        //  if(data.type_maintenance == ''){
        // sthis.toastr.error('bakit ngayyy');
        // }else{

        this.UserInfoService.createUserInfo(data).subscribe(
          (data) => {
            console.log(data);
            this.toastr.success('Successfully Inserted');
            this.spinner.show();

            setTimeout(() => {
              /** spinner ends after 5 seconds */
              this.dialogRef.close();
              this.spinner.hide();
            }, 5000);
            // this.router.navigate(['/login']);
          },
          (err) => {
            if (err instanceof HttpErrorResponse) {
              console.log(err.status);
              console.log('hello', err.error.description);
              this.toastr.error(err.error.description);
            } else {
              console.log(err);
            }
          },
          () => {
            console.log('request completed');
          }
        );

        //   }
      }

      // const username = this.formlogin.get('username').value;
      // const password = this.formlogin.get('password').value;

      // console.log('LOGIN VALUE', username, password);
    }
  } //end

  lastPeriodChange(event: MatDatepickerInputEvent<Date>) {
    const lastperiod = event.value

    const estimated_period =  lastperiod.setMonth(lastperiod.getMonth() + 8);
    this.formAddUser.get('estimated_period').patchValue(moment(estimated_period).format('MM-DD-YYYY'));

    console.log("estimated period", moment(estimated_period).format('MM-DD-YYYY'))

  }

  onDateChange(event: MatDatepickerInputEvent<Date>) {
    const bdate = moment(event.value).format();

    var timeDiff = Math.abs(Date.now() - new Date(bdate).getTime());
    const age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);

    this.formAddUser.get('age').patchValue(age);

    this.formAddUser.controls['sex'].reset();

    if (age <= 12) {
      this.showBtn = false;
      this.type = 0;
      this.child = true;
      this.showfemale = false;
      this.person_status = 'child';
      this.hidecalculate = true;
    } else if (age <= 51) {
      this.child = false;
      this.senior = true;
      this.showfemale = true;
      this.hidecalculate = true;
      this.showBtn = true;
    } else if (age >= 52) {
      this.type = 1;
      this.senior = true;
      this.child = false;
      this.showfemale = true;
      this.showBtn = true;
      this.person_status = 'senior';
    } else {
      this.type = 2;
      this.senior = true;
      this.child = false;
    }

    console.log('Teste', age);
  }

  calculate() {
    // console.log("Bmi", this.formAddUser.get('child_height').value )

    if (this.formAddUser.get('childheight').value === '') {
      this.showBtn = false;
      setTimeout(() => {
        //          this.dialogRef.close();

        this.toastr.error('Height in (m) is reqired ');
      }, 1000);
    } else if (this.formAddUser.get('childkilo').value === '') {
      this.showBtn = false;
      setTimeout(() => {
        //          this.dialogRef.close();
        this.toastr.error('Weight in (m) is reqired ');
      }, 1000);
    } else {
      const h = this.formAddUser.get('childheight').value;
      const w = this.formAddUser.get('childkilo').value;

      const totalH = h * h;

      const divideTotal = w / totalH;

      this.formAddUser.get('childmassindex').setValue(divideTotal);

      if (divideTotal >= 18.44444 && divideTotal <= 24.55555) {
        this.formAddUser.get('childstatus').setValue('Normal');
      }
      if (divideTotal <= 18.44444) {
        this.formAddUser.get('childstatus').setValue('Underweight');
      }

      if (divideTotal >= 25.0) {
        this.formAddUser.get('childstatus').setValue('Overweight');
      }

      this.hidecalculate = false;
      this.showreset = true;
      this.showBtn = true;
    }
  }

  onchangeH(event) {
    console.log('change', event.target.value);
    this.TotalValueH = event.target.value;
  }

  onchangeW(event) {
    this.TotalValueW = event.target.value;
    console.log('change', event.target.value);
  }

  resetForm() {
    this.showreset = false;
    this.hidecalculate = true;
    this.showBtn = false;
    this.formAddUser.get('childheight').setValue('');
    this.formAddUser.get('childkilo').setValue('');
    this.formAddUser.get('childmassindex').setValue('');
    this.formAddUser.get('childstatus').setValue('');
  }

  // bmiFunction(){
  //   this.TotalValueH
  //   this.TotalValueH
  //   console.log("bmiFunction", this.TotalValueH)
  // }
}
