import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePregnantDialogComponent } from './update-pregnant-dialog.component';

describe('UpdatePregnantDialogComponent', () => {
  let component: UpdatePregnantDialogComponent;
  let fixture: ComponentFixture<UpdatePregnantDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdatePregnantDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePregnantDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
