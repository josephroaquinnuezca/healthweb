import { Component, OnInit, ViewChild } from '@angular/core';
import { async } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminserviceService } from '../../service/adminservice.service';
import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog.component';
import { DeceasedDialogComponent } from './deceased-dialog/deceased-dialog.component';
import { DeleteUserDialogComponent } from './delete-user-dialog/delete-user-dialog.component';
import { EditUserDialogComponent } from './edit-user-dialog/edit-user-dialog.component';
import { UpdatePregnantDialogComponent } from './update-pregnant-dialog/update-pregnant-dialog.component';
import { ViewUserDialogComponent } from './view-user-dialog/view-user-dialog.component';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
/*

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
*/

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {




  @ViewChild('paginator', { static: false}) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort


  //mat table
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['no', 'fullname', 'age', 'sex', 'zone','medicalhistory', 'type', 'date', 'controls'];
  // dataSource = ELEMENT_DATA;


   //search
   pageOptionSize: any = [5, 10, 20];
   paginate: any = {
   totalCount!: 1100,
    pageSize: 10,
    page: 0,
    searchString: ''
  }

  //
  getAllData: any = [];

  //admins

  data_local: any;



  constructor(
    public dialog: MatDialog,
    private getalldata: AdminserviceService
    ) {}

  ngOnInit(): void {
    this.getAllClientAppointmentAdmin();
    this.data_local = JSON.parse(localStorage.getItem('sourceData')); //getting ng locato

  //  console.log("Data", this.data_local)s

   console.log("Local storage", this.data_local)

   // this.screenWidth = window.innerWidth;
  }

  //search
  onChangedPage(page: PageEvent) {
    console.log("PAGE EVENT", page)
    this.paginate.page = page.pageIndex
    this.paginate.pageSize = page.pageSize
    // console.log("PAGE LANG", page)
    this.getAllClientAppointmentAdmin();

  }
  // end

  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log("view search string", filterValue)

    this.paginator.pageIndex == 0
      ? this.getAllClientAppointmentAdmin()
      : this.paginator.firstPage();
  }

  getAllClientAppointmentAdmin() {
    this.getalldata
      .getAllUserInfo(this.paginate)
      .subscribe(async(data) => {
        console.log(data);

        var result: any = await data

        console.log("getAllClientAppointmentAdmin", result)

        this.getAllData = result.data;

        console.log("Data", this.getAllData.data)
        this.paginate.totalCount = result.data.totalCount
        this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
        this.dataSource.data = this.getAllData.data;

        // this.dataSourceAll.data = this.getAllData;
      });
  }



  adduser(){
    const dialogRef = this.dialog.open(AddUserDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getAllClientAppointmentAdmin()
      console.log(`Dialog result: ${result}`);
    });
  }

  //delete user
  deleteUser(id){
    const dialogRef = this.dialog.open(DeleteUserDialogComponent,{
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllClientAppointmentAdmin()
      console.log(`Dialog result: ${result}`);
    });
  }


  //edit User
  editUser(id){
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getAllClientAppointmentAdmin();
      console.log(`Dialog result: ${result}`);
    });
  }

  //view user
  viewUser(id){
    // const dialogRef = this.dialog.open(ViewUserDialogComponent);

    const dialogRef = this.dialog.open(ViewUserDialogComponent, {
      data: {
        id,
      },
    });


    dialogRef.afterClosed().subscribe(result => {
      this.getAllClientAppointmentAdmin()
      console.log(`Dialog result: ${result}`);
    });
  }

  deceased(id){
    // const dialogRef = this.dialog.open(ViewUserDialogComponent);

    const dialogRef = this.dialog.open(DeceasedDialogComponent, {
      data: {
        id,
      },
    });


    dialogRef.afterClosed().subscribe(result => {
      this.getAllClientAppointmentAdmin()
      console.log(`Dialog result: ${result}`);
    });
  }


  updatePregnant(id){
    // const dialogRef = this.dialog.open(ViewUserDialogComponent);

    const dialogRef = this.dialog.open(UpdatePregnantDialogComponent, {
      data: {
        id,
      },
    });


    dialogRef.afterClosed().subscribe(result => {
      this.getAllClientAppointmentAdmin()
      console.log(`Dialog result: ${result}`);
    });
  }



}
