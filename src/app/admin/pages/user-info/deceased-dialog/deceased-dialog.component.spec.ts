import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeceasedDialogComponent } from './deceased-dialog.component';

describe('DeceasedDialogComponent', () => {
  let component: DeceasedDialogComponent;
  let fixture: ComponentFixture<DeceasedDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeceasedDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeceasedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
