import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import * as moment from 'moment';

interface Gender {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-deceased-dialog',
  templateUrl: './deceased-dialog.component.html',
  styleUrls: ['./deceased-dialog.component.scss'],
})
export class DeceasedDialogComponent implements OnInit {
  genders: Gender[] = [
    { value: 'Male', viewValue: 'Male' },
    { value: 'Female', viewValue: 'Female' },
  ];
  //add
  formAddUser: FormGroup;

  dialogData: any;

  constructor(
    public dialogRef: MatDialogRef<DeceasedDialogComponent>,
    public UserInfoService: AdminserviceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    private spinner: NgxSpinnerService,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.dialogData = results;
  }

  ngOnInit(): void {
    this.ViewSingleData();
    this.AddUserFormValidation();
  }

  onSubmit() {

    if (this.formAddUser.invalid) {
      this.formAddUser.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      return;
    } else {


      const data = {

        fname: this.formAddUser.get('fname').value,
        mname: this.formAddUser.get('mname').value,
        lname: this.formAddUser.get('lname').value,
        suffix: this.formAddUser.get('suffix').value,
        bdate: this.formAddUser.get('bdate').value,
        age: this.formAddUser.get('age').value,
        sex: this.formAddUser.get('sex').value,

      }

      const stringify = JSON.stringify(data);
      console.log("With Stringify :" , stringify);


      console.log(data)


    this.UserInfoService.updateDeceasedUser(data, this.dialogData.id).subscribe(data =>{
          console.log(data);
          this.toastr.success('Successfully Inserted');
          this.spinner.show();

          setTimeout(() => {
            /** spinner ends after 5 seconds */
            this.dialogRef.close();
            this.spinner.hide();
          }, 5000);
          // this.router.navigate(['/login']);
        }, err => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status)
            console.log("hello",err.error.description)
            this.toastr.error(err.error.description);
          }else{
            console.log(err)
          }

        }, () => {
          console.log('request completed')
        });



    }


  }

  onDateChange(event: MatDatepickerInputEvent<Date>) {
    const bdate = moment(event.value).format();

    var timeDiff = Math.abs(Date.now() - new Date(bdate).getTime());
    const age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);

    this.formAddUser.get('age').patchValue(age);

    console.log('Teste', age);
  }

  AddUserFormValidation() {
    this.formAddUser = this.formgroup.group({
      fname: [''],
      mname: [''],
      lname: [''],
      suffix: [''],
      bdate: [''],
      age: [''],
      sex: [''],
      // house_no: [''],
      // zone: [''],
      // province: [''],
      // city: [''],
      // barangay: [''],
      // cell_no: ['',
      // [ Validators.required, Validators.pattern('^[0-9]*$'),
      // Validators.minLength(11),
      // Validators.maxLength(11)]],
      // childheight: [''],
      // childkilo: [''],
      // childmassindex: [''],
      // maintenance_senior: [''],
      // maintenance_pregnant: [''],
      // pregnant_status: [''],
      // type_maintenance  : [''],
      // last_period: [''],
    });
  }

  ViewSingleData() {
    const id = this.dialogData.id;

    this.UserInfoService.ViewSingleUserInfo(id).subscribe(
      (data) => {
        console.log('Get Single data', data);

        const dataform: any = data;

        this.formAddUser.get('fname').patchValue(dataform?.data.fname);
        this.formAddUser.get('mname').patchValue(dataform?.data.mname);
        this.formAddUser.get('lname').patchValue(dataform?.data.lname);
        this.formAddUser.get('suffix').patchValue(dataform?.data.suffix);
        this.formAddUser.get('bdate').patchValue(moment(dataform?.data.bdate).format('MM-DD-YYYY'));
        this.formAddUser.get('age').patchValue(dataform?.data.age);
        this.formAddUser.get('sex').patchValue(dataform?.data.sex);
        // this.formAddUser.get('house_no').patchValue(dataform?.data.house_no);
        // this.formAddUser.get('zone').patchValue(dataform?.data.zone);
        // this.formAddUser.get('city').patchValue(dataform?.data.city);
        // this.formAddUser.get('province').patchValue(dataform?.data.province);
        // this.formAddUser.get('barangay').patchValue(dataform?.data.barangay);
        // this.formAddUser.get('cell_no').patchValue(dataform?.data.cell_no);

        // this.formAddUser.get("child_height").patchValue(dataform?.data.child_height);
        // this.formAddUser.get("child_weight").patchValue(dataform?.data.child_weight);
        // this.formAddUser.get("child_body_mass_index").patchValue(dataform?.data.child_body_mass_index);
        // this.formAddUser.get("type").patchValue(dataform?.data.type);
        // this.formAddUser.get("maintenance_senior").patchValue(dataform?.data.maintenance_senior);
        // this.formAddUser.get("maintenance_pregnant").patchValue(dataform?.data.maintenance_pregnant);
        // this.formAddUser.get("pregnant_status").patchValue(dataform?.data.pregnant_status);
        // this.formAddUser.get("last_period").patchValue(dataform?.data.last_period);
        // this.formAddUser.get("type_maintenance").patchValue(dataform?.data.type_maintenance);

        // fname: this.formAddUser.get('fname').value,
        // mname: this.formAddUser.get('mname').value,
        // lname: this.formAddUser.get('lname').value,
        // suffix: this.formAddUser.get('suffix').value,
        // bdate: this.formAddUser.get('bdate').value,
        // age: this.formAddUser.get('age').value,
        // sex: this.formAddUser.get('sex').value,
        // house_no: this.formAddUser.get('house_no').value,
        // zone: this.formAddUser.get('zone').value,
        // city: this.formAddUser.get('city').value,
        // province: this.formAddUser.get('province').value,
        // barangay: this.formAddUser.get('barangay').value,
        // cell_no: this.formAddUser.get('cell_no').value,
        // child_height: this.formAddUser.get('childheight').value,
        // child_weight: this.formAddUser.get('childkilo').value,
        // child_body_mass_index: this.formAddUser.get('childmassindex').value,
        // type: this.type,
        // maintenance_senior: this.formAddUser.get('maintenance_senior').value,
        // maintenance_pregnant: this.formAddUser.get('maintenance_pregnant').value,
        // pregnant_status: this.formAddUser.get('pregnant_status').value ,
        // last_period: this.formAddUser.get('last_period').value,
        // type_maintenance: this.formAddUser.get('type_maintenance').value,
      },
      (err) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status);
          console.log('hello', err.error.description);
          this.toastr.error(err.error.description);
        } else {
          console.log(err);
        }
      },
      () => {
        console.log('request completed');
      }
    );
  }
}
