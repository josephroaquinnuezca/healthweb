import { AddPregMaintenanceComponent } from './update-female-mens/add-preg-maintenance/add-preg-maintenance.component';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import { UpdateChildDialogComponent } from './update-child-dialog/update-child-dialog.component';
import { UpdateFemaleMensComponent } from './update-female-mens/update-female-mens.component';

import * as moment from 'moment';
import { PatientAddDialogComponent } from '../../patient/patient-add-dialog/patient-add-dialog.component';
@Component({
  selector: 'app-view-user-dialog',
  templateUrl: './view-user-dialog.component.html',
  styleUrls: ['./view-user-dialog.component.scss'],
})
export class ViewUserDialogComponent implements OnInit {
  // displayedColumnsWomen: string[] = ['position', 'name', 'weight', 'symbol'];
  // dataSource = ELEMENT_DATA;

  date: Date;
  send_date: Date;

  formattedDate: any;

  @ViewChild('paginator', { static: false }) paginator!: MatPaginator;
  @ViewChild('dataSort', { read: MatSort, static: false }) sort!: MatSort;

  //mat table

  dataSourceWomen: MatTableDataSource<any>;
  displayedColumnsWomens: string[] = ['no', 'mens_date', 'status', 'controls'];

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = [
    'no',
    'fullname',
    'height',
    'weight',
    'bmi',
    'status',
    'date',
    'controls',
  ];
  // dataSource = ELEMENT_DATA;

  //search
  pageOptionSize: any = [5, 10, 20];
  paginate: any = {
    totalCount: 1100,
    pageSize: 10,
    page: 0,
    searchString: '',
  };

  dialogData: any;

  DataValues: any;

  ShowNormal = false;
  ShowOverWeight = false;
  ShowUnderWeight = false;

  //
  getAllData: any = [];

  getAllDataWomen: any = [];

  constructor(
    // private clientService: ClientServiceService,
    private getalldata: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
    public dialog: MatDialog
  ) {
    this.dialogData = results;
  }

  ngOnInit(): void {
    this.fetchAllData();

    console.log(this.dialogData.id);

    this.getAllChildStatus();

    this.fetchSingleDataWomen();
  }

  fetchAllData() {
    this.getalldata.ViewSingleUserInfo(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data;

      //for women check up

      //test date

      const test_date = this.DataValues.last_period;

      const format = new Date();



      // ||  parseFloat(this.getAllData.child_body_mass_index) == 24.9

      console.log('parse float', this.DataValues);

      if (
        parseFloat(this.DataValues.child_body_mass_index) >= 18.55555 ||
        parseFloat(this.getAllData.child_body_mass_index) == 24.99999
      ) {
        this.ShowNormal = true;
        this.ShowOverWeight = false;
        this.ShowUnderWeight = false;
      }

      // else if(result.child_body_mass_index > 25 ){
      //   this.ShowUnderWeight = false;
      //   this.ShowNormal = false;
      //   this.ShowOverWeight = true;
      // }else if(result.child_body_mass_index < 8.4 ){
      //   this.ShowNormal = false;
      //   this.ShowOverWeight = false;
      //   this.ShowUnderWeight = true
      // }

      console.log('results', result);

      // this.getAllClient();
    });
  }

  updateDialog(id) {
    const dialogRef = this.dialog.open(UpdateChildDialogComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      // this.getAllClientAppointmentAdmin();
      this.fetchAllData();
      console.log(`Dialog result: ${result}`);
    });
  }

  updateWomenMens(id) {
    const dialogRef = this.dialog.open(UpdateFemaleMensComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      // this.getAllClientAppointmentAdmin();
      this.fetchAllData();
      console.log(`Dialog result: ${result}`);
    });
  }

  addpregMaintenance(id) {
    const dialogRef = this.dialog.open(AddPregMaintenanceComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      // this.getAllClientAppointmentAdmin();
      this.fetchAllData();
      console.log(`Dialog result: ${result}`);
    });
  }

  seniorMaintenance(id) {
    const dialogRef = this.dialog.open(PatientAddDialogComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      // this.getAllClientAppointmentAdmin();
      this.fetchAllData();
      console.log(`Dialog result: ${result}`);
    });
  }

  // start 03/11/2022

  //search
  onChangedPage(page: PageEvent) {
    console.log('PAGE EVENT', page);
    this.paginate.page = page.pageIndex;
    this.paginate.pageSize = page.pageSize;
    // console.log("PAGE LANG", page)
    this.getAllChildStatus();
  }
  // end

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.paginate.searchString = filterValue;
    console.log('view search string', filterValue);

    this.paginator.pageIndex == 0
      ? this.getAllChildStatus()
      : this.paginator.firstPage();
  }

  getAllChildStatus() {
    this.getalldata
      .getAllChildStatusHistory(this.paginate, this.dialogData.id)
      .subscribe(async (data) => {
        console.log(data);

        var result: any = await data;

        console.log('getAllChildStatus', result);

        this.getAllData = result.data;

        console.log('Data', this.getAllData.data);
        this.paginate.totalCount = result.data.totalCount;
        this.dataSource = new MatTableDataSource<any>(this.getAllData.data);
        this.dataSource.data = this.getAllData.data;

        // this.dataSourceAll.data = this.getAllData;
      });
  }
  //womens
  fetchSingleDataWomen() {
    this.getalldata
      .getAllWomensPeriod(this.paginate, this.dialogData.id)
      .subscribe(async (data) => {
        console.log(data);

        var result: any = await data;

        this.getAllDataWomen = result.data;

        this.paginate.totalCount = result.data.totalCount;
        this.dataSourceWomen = new MatTableDataSource<any>(
          this.getAllDataWomen.data
        );
        this.dataSourceWomen.data = this.getAllDataWomen.data;

        // this.dataSourceAll.data = this.getAllData;
      });
  }
  //ends

  cancel(){

    this.getalldata
    .getAllWomensPeriod(this.paginate, this.dialogData.id)
    .subscribe(async (data) => {
      console.log(data);

      var result: any = await data;

      this.getAllDataWomen = result.data;

      this.paginate.totalCount = result.data.totalCount;
      this.dataSourceWomen = new MatTableDataSource<any>(
        this.getAllDataWomen.data
      );
      this.dataSourceWomen.data = this.getAllDataWomen.data;

      // this.dataSourceAll.data = this.getAllData;
    });

  }

  confirm(){

    this.getalldata
    .getAllWomensPeriod(this.paginate, this.dialogData.id)
    .subscribe(async (data) => {
      console.log(data);

      var result: any = await data;

      this.getAllDataWomen = result.data;

      this.paginate.totalCount = result.data.totalCount;
      this.dataSourceWomen = new MatTableDataSource<any>(
        this.getAllDataWomen.data
      );
      this.dataSourceWomen.data = this.getAllDataWomen.data;

      // this.dataSourceAll.data = this.getAllData;
    });

  }





}
