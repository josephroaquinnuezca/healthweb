import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, EventEmitter, Inject, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

@Component({
  selector: 'app-update-female-mens',
  templateUrl: './update-female-mens.component.html',
  styleUrls: ['./update-female-mens.component.scss']
})
export class UpdateFemaleMensComponent implements OnInit {




  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();


  dialogData: any;

  DataValues: any;

  //add
  formAddUser: FormGroup;

  hidecalculate = true
  showBtn = false;

  constructor(
    public dialogRef: MatDialogRef<UpdateFemaleMensComponent>,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    private getSingledata: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.dialogData = results;
  }

  ngOnInit(): void {
    this.getSingleData();

    this.formAddUser = this.formgroup.group({
      user_id: [''],
      womens_date: ['',  Validators.required],
      cell_no: [''],
      comment: ['',  Validators.required],

    });

  }

  //
  get f() {
    return this.formAddUser.controls;
  }

  getSingleData(){
    this.getSingledata.ViewSingleUserInfo(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data


      this.formAddUser.get("childheight").patchValue(this.DataValues?.child_height);
      this.formAddUser.get("childkilo").patchValue(this.DataValues?.child_weight);
      this.formAddUser.get("childmassindex").patchValue(this.DataValues?.child_body_mass_index);

      console.log("results", result)

      // this.getAllClient();
    });

  }





  onSubmit() {
    if (this.formAddUser.invalid) {

      this.formAddUser.markAllAsTouched();
      this.toastr.error('Please complete all required fields');

      return;
    } else {

      const data = {

        user_id: this.formAddUser.get('user_id').value,
        womens_date:this.formAddUser.get('womens_date').value,
        cell_no: this.formAddUser.get('cell_no').value,
        comment:this.formAddUser.get('comment').value,


      }

      // const stringify = JSON.stringify(data);
      // console.log("With Stringify :" , stringify);
      // console.log("id :" , this.dialogData.id);

      console.log(data)


      this.getSingledata.createFollowCheckup(data).subscribe(data =>{
        console.log(data);
        this.toastr.success('Successfully Updated');
        this.dialogRef.close();

        // this.router.navigate(['/login']);
      }, err => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status)
          console.log("hello",err.error.description)
          this.toastr.error(err.error.description);
        }else{
          console.log(err)
        }

      }, () => {
        console.log('request completed')
      });




    }
  }//end

  onDateChange(event:  MatDatepickerInputEvent<Date>) {

    const bdate = moment(event.value).format();

    var timeDiff = Math.abs(Date.now() - new Date(bdate).getTime());
    const age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);

    this.formAddUser.get("age_update").patchValue(age);

  }


}
