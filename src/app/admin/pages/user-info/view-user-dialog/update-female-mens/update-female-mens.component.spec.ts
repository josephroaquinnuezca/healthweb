import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFemaleMensComponent } from './update-female-mens.component';

describe('UpdateFemaleMensComponent', () => {
  let component: UpdateFemaleMensComponent;
  let fixture: ComponentFixture<UpdateFemaleMensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateFemaleMensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFemaleMensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
