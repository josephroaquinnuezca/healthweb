import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPregMaintenanceComponent } from './add-preg-maintenance.component';

describe('AddPregMaintenanceComponent', () => {
  let component: AddPregMaintenanceComponent;
  let fixture: ComponentFixture<AddPregMaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPregMaintenanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPregMaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
