import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
import { AdminInterface } from 'src/app/shared/interface/admin-interface';

@Component({
  selector: 'app-update-child-dialog',
  templateUrl: './update-child-dialog.component.html',
  styleUrls: ['./update-child-dialog.component.scss'],
})
export class UpdateChildDialogComponent implements OnInit {


  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();


  dialogData: any;

  DataValues: any;

  //add
  formAddUser: FormGroup;

  hidecalculate = true
  showBtn = false;

  constructor(
    public dialogRef: MatDialogRef<UpdateChildDialogComponent>,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    private getSingledata: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results
  ) {
    this.dialogData = results;
  }

  ngOnInit(): void {
    this.getSingleData();

    this.formAddUser = this.formgroup.group({
      childheight: [''],
      childkilo: [''],
      childmassindex: [''],
      childheight_update: ['',  Validators.required],
      childkilo_update: ['',  Validators.required],
      childmassindex_update: [''],
      bdate_update: ['',  Validators.required],
      age_update: [''],

    });

  }

  //
  get f() {
    return this.formAddUser.controls;
  }

  getSingleData(){
    this.getSingledata.ViewSingleUserInfo(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data


      this.formAddUser.get("childheight").patchValue(this.DataValues?.child_height);
      this.formAddUser.get("childkilo").patchValue(this.DataValues?.child_weight);
      this.formAddUser.get("childmassindex").patchValue(this.DataValues?.child_body_mass_index);

      console.log("results", result)

      // this.getAllClient();
    });

  }


  calculate(){


    // console.log("Bmi", this.formAddUser.get('child_height').value )



    if(this.formAddUser.get('childheight_update').value === ""){

      this.showBtn = false;
      setTimeout (() => {
        //          this.dialogRef.close();

        this.toastr.error('Height in (m) is reqired ');
              }, 1000);

    } else if (this.formAddUser.get('childkilo_update').value === "" ){
      this.showBtn = false;
      setTimeout (() => {
        //          this.dialogRef.close();
        this.toastr.error('Weight in (m) is reqired ');
              }, 1000);
    }else {

      ///fuckme

      const h = this.formAddUser.get('childheight_update').value
      const w =  this.formAddUser.get('childkilo_update').value

      const totalH = h * h;

      const divideTotal =  w / totalH


      this.formAddUser.get("childmassindex_update").setValue(divideTotal);

      this.hidecalculate = false;
      this.showBtn = true;

      // /shitt
    }




  }


  onSubmit() {
    if (this.formAddUser.invalid) {

      this.formAddUser.markAllAsTouched();
      this.toastr.error('Please complete all required fields');

      return;
    } else {

      const data = {

        fname: this.DataValues?.fname,
        mname: this.DataValues?.mname,
        lname:this.DataValues?.lname,
        suffix:this.DataValues?.suffix,
        bdate: this.DataValues?.bdate,
        age: this.DataValues?.age,
        bdate_update: this.formAddUser.get('bdate_update').value,
        age_update: this.formAddUser.get('age_update').value,
        sex: this.DataValues?.sex,
        house_no: this.DataValues?.house_no,
        zone: this.DataValues?.zone,
        city: this.DataValues?.city,
        province: this.DataValues?.province,
        barangay: this.DataValues?.barangay,
        cell_no: this.DataValues?.cell_no,
        child_height: this.DataValues?.child_height,
        child_weight: this.DataValues?.child_weight,
        child_body_mass_index: this.DataValues?.child_body_mass_index,
        child_height_update:this.formAddUser.get('childheight_update').value,
        child_weight_update: this.formAddUser.get('childkilo_update').value,
        child_body_mass_index_update: this.formAddUser.get('childmassindex_update').value,


      }

      const stringify = JSON.stringify(data);
      console.log("With Stringify :" , stringify);
      console.log("id :" , this.dialogData.id);

      console.log(data)


      this.getSingledata.updateChildStatus(data, this.dialogData.id).subscribe(data =>{
        console.log(data);
        this.toastr.success('Successfully Updated');
        this.dialogRef.close();

        // this.router.navigate(['/login']);
      }, err => {
        if (err instanceof HttpErrorResponse) {
          console.log(err.status)
          console.log("hello",err.error.description)
          this.toastr.error(err.error.description);
        }else{
          console.log(err)
        }

      }, () => {
        console.log('request completed')
      });




    }
  }//end

  onDateChange(event:  MatDatepickerInputEvent<Date>) {

    const bdate = moment(event.value).format();

    var timeDiff = Math.abs(Date.now() - new Date(bdate).getTime());
    const age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);

    this.formAddUser.get("age_update").patchValue(age);

  }

}
