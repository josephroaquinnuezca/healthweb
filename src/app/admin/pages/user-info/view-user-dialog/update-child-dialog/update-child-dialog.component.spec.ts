import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateChildDialogComponent } from './update-child-dialog.component';

describe('UpdateChildDialogComponent', () => {
  let component: UpdateChildDialogComponent;
  let fixture: ComponentFixture<UpdateChildDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateChildDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateChildDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
