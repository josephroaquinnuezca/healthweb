import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';

@Component({
  selector: 'app-delete-user-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.scss']
})
export class DeleteUserDialogComponent implements OnInit {

  dialogData:any;

  DataValues:any;

  constructor(
    // private clientService: ClientServiceService,
    public dialogRef: MatDialogRef<DeleteUserDialogComponent>,
    private getalldata: AdminserviceService,
    @Inject(MAT_DIALOG_DATA) public results,
  ) {
    this.dialogData = results
  }


  ngOnInit(): void {



    console.log(this.dialogData.id)
  }

  onSubmit(){



    this.getalldata.DeleteSingleUserInfo(this.dialogData.id).subscribe((data) => {
      console.log(data);

      var result: any = data;

      this.DataValues = result.data

      console.log("results", result)

      this.dialogRef.close();

      // this.getAllClient();
    });
  }

}
