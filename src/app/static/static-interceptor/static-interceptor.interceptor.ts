import { catchError, finalize } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { TokenServiceService } from '../static-service/token-service.service';
import { StaticServiceService } from '../static-service/static-service.service';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class StaticInterceptorInterceptor implements HttpInterceptor {

  // constructor() {}

  // intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
  //   return next.handle(request);
  // }
    // constructor() {}

  // intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
  //   return next.handle(request);
  // }
  //start
  constructor(
    private token: TokenServiceService,
    private router: Router,
    private dialogRef: MatDialog,
    private userService: StaticServiceService
    ) { }

intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    const token = this.token.getToken();

    if (token != null) {
        authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });
    }
    // This will use the cookie set by the server by default
    authReq = req.clone({
        withCredentials: true
    });
    // this.reusableDialogsService.openDialogLoading();
    console.log("Auth intercepter test");
    // return next.handle(authReq);
    return next.handle(authReq)
    // .pipe(catchError((error, caught) => {
    //     console.log(error)
    //     console.log(caught)
        // if (error instanceof HttpErrorResponse) {
        //   console.log(error.status)
        //   if ([403, 415].includes(error.status)) {
            // const dialogRef = this.dialogRef.open(ErrorDialogComponent, {
            //     panelClass: 'app-full-bleed-dialog-p-26',
            //     disableClose: false,
            //     data: 'Your session is expired. Please signin again.'
            // });
            // dialogRef.afterClosed().subscribe(result => {
            //  if (localStorage.getItem('dataSource')) {
            //     this.dialogRef.closeAll();
            //     this.userService.logout();
            //     this.router.navigate(['/admin/signin']);
            //   }

            // });
        //   }
        // }
        // this.handleError(error)
    //     return throwError(error);
    // }),
    // finalize(()=>{
        // this.reusableDialogsService.closeDialogLoading();
    // })
    // ) as Observable<HttpEvent<any>>;
}
  //end
}

// This can be export without []
export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: StaticInterceptorInterceptor, multi: true }
];
