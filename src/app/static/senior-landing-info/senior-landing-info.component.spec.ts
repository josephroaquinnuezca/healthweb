import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeniorLandingInfoComponent } from './senior-landing-info.component';

describe('SeniorLandingInfoComponent', () => {
  let component: SeniorLandingInfoComponent;
  let fixture: ComponentFixture<SeniorLandingInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeniorLandingInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeniorLandingInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
