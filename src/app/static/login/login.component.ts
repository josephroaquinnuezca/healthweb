import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StaticServiceService } from '../static-service/static-service.service';
import { TokenServiceService } from '../static-service/token-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  formlogin: FormGroup;
  submitted = false;

  dataError: any;

  localData: any = [];
  // formgroup: any;

  constructor(
    private tokenStorage: TokenServiceService,
    public loginService: StaticServiceService,
    private toastr: ToastrService,
    private formgroup: FormBuilder,
    private router: Router,
  ) {
    // this.loginForm = this.loginFormValidation();
  }

  ngOnInit() {
    this.loginFormValidation();
  }

  get f() {
    return this.formlogin.controls;
  }

  loginFormValidation() {
    this.formlogin = this.formgroup.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.formlogin.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  onSubmit() {
    if (this.formlogin.invalid) {
      //this is to show all the required fields
      this.formlogin.markAllAsTouched();
      this.toastr.error('Please complete all required fields');
      return;
    } else {

      const username = this.formlogin.get('username').value;
      const password = this.formlogin.get('password').value;

      console.log('LOGIN VALUE', username, password);

      this.loginService.login(username, password).subscribe(
        (data) => {
          console.log('data login', data);

          const result: any = data;

          console.log(result.data.token);




          this.tokenStorage.saveToken(result.data.token);

          const newToken = this.tokenStorage.decodeToken(result.data.token);

          window.localStorage.setItem('sourceData', JSON.stringify(newToken['auth_data'])) //saving in local storage
          console.log("new token", newToken)

          // this.router.navigate(['/public/customer-dashboard']);

           if (newToken['auth_data'].role == 0 || newToken['auth_data'].role == 1 || newToken['auth_data'].role == 2 || newToken['auth_data'].role == 3) {
              this.toastr.success('Successfully Login');
              this.router.navigate(['/pages/admin/dashboard']);
            }



          },





          // localStorage.setItem(
          //   'dataSource',
          //   JSON.stringify(newToken.auth_data)
          // );

          // console.log('new token', newToken);


        (err) => {
          if (err instanceof HttpErrorResponse) {
            console.log(err.status);
            console.log('hello', err.error.description);
            this.toastr.error(err.error.description);
          } else {
            console.log(err);
          }
        },
        () => {
          console.log('request completed');
        }
      );
    }
  }


}


// this.loginService.login(username, password).subscribe(
//   (data) => {
//     console.log('data login', data);

//     const result: any = data;

//     console.log(result.data.token);




//     this.tokenStorage.saveToken(result.data.token);

//     const newToken = this.tokenStorage.decodeToken(result.data.token);

//     window.localStorage.setItem('sourceData', JSON.stringify(newToken['auth_data'])) //saving in local storage
//     console.log("new token", newToken)

//     // this.router.navigate(['/public/customer-dashboard']);

//      if (newToken['auth_data'].role == 0) {
//         this.toastr.success('Successfully Login');
//         this.router.navigate(['/public/admin-dashboard']);
//       } else {
//         this.toastr.success('Successfully Login');
//         this.router.navigate(['/public/customer-dashboard']);
//       }
//     },





//     // localStorage.setItem(
//     //   'dataSource',
//     //   JSON.stringify(newToken.auth_data)
//     // );

//     // console.log('new token', newToken);


//   (err) => {
//     if (err instanceof HttpErrorResponse) {
//       console.log(err.status);
//       console.log('hello', err.error.description);
//       this.toastr.error(err.error.description);
//     } else {
//       console.log(err);
//     }
//   },
//   () => {
//     console.log('request completed');
//   }
// ); //end
