import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PregnantLandingInfoComponent } from './pregnant-landing-info.component';

describe('PregnantLandingInfoComponent', () => {
  let component: PregnantLandingInfoComponent;
  let fixture: ComponentFixture<PregnantLandingInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PregnantLandingInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PregnantLandingInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
