import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import jwt_decode from "jwt-decode";

const TOKEN_KEY = 'Bearer';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenServiceService {

  constructor(private router: Router) { }

  signOut(): void {
      window.sessionStorage.clear();
      this.router.navigate(['/admin/signin']);
  }

  public saveToken(token: string): void {
      window.sessionStorage.removeItem(TOKEN_KEY);
      window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
      return window.sessionStorage.getItem(TOKEN_KEY);
  }

    decodeToken(token: string) {

      // return jwt_decode(token)
      const _decodeToken = (token: string) => {
          try {
              return JSON.parse(atob(token));
          } catch {
              return;
          }
      };
      return token
          .split('.')
          .map(token => _decodeToken(token))
          .reduce((acc, curr) => {
              if (!!curr) acc = { ...acc, ...curr };
              return acc;
          }, Object.create(null));
  }
}
