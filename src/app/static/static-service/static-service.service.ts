import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaticServiceService {


  //  HTTP_API_URL_PRIVATE = "http://localhost:4100/v1/private/users/";
  //  HTTP_API_URL_PUBLIC = "http://localhost:4100/v1/public/users/";


 HTTP_API_URL_PRIVATE = "https://health-care-api.onrender.com/v1/private/users/";
 HTTP_API_URL_PUBLIC = "https://health-care-api.onrender.com/v1/public/users/";

constructor(private http: HttpClient) { }




login(username: string, password: string){
  let API_URL = `${this.HTTP_API_URL_PUBLIC}login`;
  return this.http.post(API_URL, { username, password })


}

logout(): Observable<any>{
  return this.http.get(this.HTTP_API_URL_PUBLIC + '/logout')
  }


}
