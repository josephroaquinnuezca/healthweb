import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, filter, scan } from 'rxjs/operators';
import { AdminserviceService } from 'src/app/admin/service/adminservice.service';
@Component({
  selector: 'app-child-landing-info',
  templateUrl: './child-landing-info.component.html',
  styleUrls: ['./child-landing-info.component.scss']
})
export class ChildLandingInfoComponent implements OnInit {

  unique_code: any;
  data: any;


  constructor(
    private route: ActivatedRoute,
    public UserInfoService: AdminserviceService
    ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {

    this.unique_code = params['unique_code'];

    console.log(params['unique_code'])


    this.UserInfoService.getSingleUniqueCode(params['unique_code']).subscribe(data =>{
      console.log("Get Single data", data);

      this.data = data.data

    }, err => {
      if (err instanceof HttpErrorResponse) {
        console.log(err.status)
        console.log("hello",err.error.description)
      }else{
        console.log(err)
      }

    }, () => {
      console.log('request completed')
    });





  });


  }

}
