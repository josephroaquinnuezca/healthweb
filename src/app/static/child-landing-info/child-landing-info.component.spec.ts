import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildLandingInfoComponent } from './child-landing-info.component';

describe('ChildLandingInfoComponent', () => {
  let component: ChildLandingInfoComponent;
  let fixture: ComponentFixture<ChildLandingInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildLandingInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildLandingInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
