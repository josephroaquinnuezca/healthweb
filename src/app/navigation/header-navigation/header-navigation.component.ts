import { animate, animation, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NavigationEnd, Router } from '@angular/router';
import { LogoutComponent } from 'src/app/static/logout/logout.component';

interface SideNavToggle {
  screenWidth: number;
  collapsed: boolean;
}



@Component({
  selector: 'app-header-navigation',
  templateUrl: './header-navigation.component.html',
  styleUrls: ['./header-navigation.component.scss'],
  animations: [
    trigger('fadeOut', [
      transition(':enter',[
        style({
          opacity:0
        }),
        animate('350ms',
          style({
            opacity: 1
          })
        )
      ]),
      transition(':leave',[
        style({
          opacity:1
        }),
        animate('350ms',
          style({
            opacity: 0
          })
        )
      ]),
    ])
  ]
})
export class HeaderNavigationComponent implements OnInit {

  @Output() onToggleSideNav: EventEmitter<SideNavToggle> = new EventEmitter();

  screenWidth = 0;


  collapsed = false;

  isSideNavCollapsed = false;

  data_local: any;



  // OnInit(){
  //  console.log("Router",  this.router.url)
  // }




  @HostListener('window:resize', ['$event'])
  onResize(event: any){
    this.screenWidth = window.innerWidth
    if(this.screenWidth <= 768){
      this.collapsed = false
      this.onToggleSideNav.emit({
        collapsed: this.collapsed,
        screenWidth: this.screenWidth
      })
    }
  }

  currentRoute: string;
  show = false;

  constructor(
    private router: Router,
    public dialog: MatDialog
    ){

    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if(e.url == "/login"){
          this.show = false
        }else {
          this.show = true
        }
      }
    });
  }

  ngOnInit(): void {


   this.data_local = JSON.parse(localStorage.getItem('sourceData')); //getting ng locato

  //  console.log("Data", this.data_local)s

   console.log("Local storage", this.data_local)

    this.screenWidth = window.innerWidth;
  }



  toggleBrand(){
    this.collapsed = !this.collapsed
    this.onToggleSideNav.emit({
      collapsed: this.collapsed,
      screenWidth: this.screenWidth
    })
  }

  closeSideNav(){
    this.collapsed = false
  }

  logout(){
    const dialogRef = this.dialog.open(LogoutComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }



}
