import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';


interface SideNavToggle {
  screenWidth: number;
  collapsed: boolean;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'health-care-web';

  currentRoute: string;
  show = false;

  constructor(private router: Router){

    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if(e.url == "/login"){
          this.show = false
        }else {
          this.show = true
        }
      }
    });
  }



  isSideNavCollapsed = false;
  screenWidth = 0;



  // OnInit(){
  //  console.log("Router",  this.router.url)
  // }


  onToggleSideNav(data: SideNavToggle){
    this.screenWidth = data.screenWidth;
    this.isSideNavCollapsed = data.collapsed;
  }




}
